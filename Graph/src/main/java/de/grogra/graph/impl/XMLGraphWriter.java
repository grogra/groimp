/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.graph.impl;

import java.io.IOException;

import de.grogra.util.IOWrapException;
import de.grogra.xl.util.ObjectList;
import de.grogra.persistence.*;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class XMLGraphWriter extends XMLPersistenceWriter implements GraphOutput {
	private final AttributesImpl atts = new de.grogra.util.SAXElement();
	private final StringBuffer buf = new StringBuffer();
	private GraphManager manager;
	private int visitedMark;
	private ObjectList nodesToWrite;
	private boolean onlyReferences;
	
	private boolean ignored = false; // current node is ignored
	private Filter filter;
	private ObjectList ignoredNodes;
	
	/**
	 * Filter nodes either by returning null -> node not considered.
	 * Or by changing the attribute on how to write it.
	 */
	public interface Filter
	{
		AttributesImpl filter(Node node, Attributes attr);
	}


	private void add(String name, String value) {
		atts.addAttribute("", name, name, "CDATA", value);
	}

	public XMLGraphWriter(ContentHandler ch, PersistenceOutputListener listener) {
		this(ch, listener, false);
		filter = null;
	}

	public XMLGraphWriter(ContentHandler ch, PersistenceOutputListener listener, boolean onlyReferences) {
		super(ch, listener);
		this.onlyReferences = onlyReferences;
		filter = null;
	}
	
	public XMLGraphWriter(ContentHandler ch, PersistenceOutputListener listener, 
			boolean onlyReferences, Filter f) {
		super(ch, listener);
		this.onlyReferences = onlyReferences;
		filter = f;
	}

	public void beginExtent(GraphManager manager, int rootCount)
			throws IOException {
		beginExtent(manager);
		atts.setAttributes(NS_ATTRIBUTE);
		try {
			getContentHandler().startPrefixMapping(NS_PREFIX, NAMESPACE);
		} catch (SAXException e) {
			throw new IOWrapException(e);
		}
		startElement("graph", atts);
		this.manager = manager;
		visitedMark = manager.allocateBitMark(false);
		nodesToWrite = new ObjectList();
		ignoredNodes = new ObjectList();
	}

	@Override
	public void endExtent() throws IOException {
		while (!nodesToWrite.isEmpty()) {
			Node n = (Node) nodesToWrite.pop();
			if (!n.getBitMark(visitedMark)) {
				beginNode(n, null);
				endNode(n);
			}
		}
		nodesToWrite = null;
		manager.disposeBitMark(visitedMark, true);
		manager = null;
		endElement("graph");
		try {
			getContentHandler().endPrefixMapping(NS_PREFIX);
		} catch (SAXException e) {
			throw new IOWrapException(e);
		}
		super.endExtent();
	}

	private String rootName;

	public void beginRoot(String name) throws IOException {
		rootName = name;
	}

	public void endRoot(String name) throws IOException {
	}

	public void beginNode(Node node, Edge edge) throws IOException {
		atts.clear();
		// As extent can have cycles, and several paths can lead to one node this is pretty bad way
		// of ignoring some nodes. Should not be used other from the meta graph export
		if (ignored) {
			node.setBitMark(visitedMark, true);
			ignoredNodes.push(node);
			return;
		}
		Attributes a = (filter != null) ? filter.filter(node, atts) : atts;
		if (a == null) {
			ignored = true;
			node.setBitMark(visitedMark, true);
			ignoredNodes.push(node);
			return; 
		}
		ignored = false;
		
		//
		if (rootName != null) {
			add("root", rootName);
			rootName = null;
		}
		boolean firstVisit = !node.setBitMark(visitedMark, true) && !onlyReferences;
		if (firstVisit) {
			add("id", Long.toString(node.getId()));
			add("type", node.getNType().getBinaryName());
			if (listener != null) {
				listener.objectWritten(node);
			}
		} else {
			add("ref", Long.toString(node.getId()));
		}
		if (edge != null) {
			buf.setLength(0);
			edge.getEdgeKeys(buf, true, false);
			add("edges", buf.toString());
		}
		startElement("node", atts);
		if (firstVisit) {
			writeFields(node);
		}
	}

	public void endNode(Node node) throws IOException {
		if (ignored) { 
			ignoredNodes.pop();
			if (ignoredNodes.isEmpty()) {
				ignored = false;
			}
			return; 
		}
		endElement("node");
	}
	
	@Override
	public void writePersistentObjectReference(PersistenceCapable o)
			throws IOException {
		super.writePersistentObjectReference(o);
//		if (o.getPersistenceManager() == manager) { // only try to save node managed by current manager
			if (!((Node) o).getBitMark(visitedMark)) {
				nodesToWrite.add(o);
			}
//		}
	}

}
