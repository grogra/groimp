---
ftitle: 2D Plugin Manual
...


# IMP-2D plugin 

IMP-2D provides node classes for 2D geometry. Such a 2D scene graph can be displayed in a 2D view, but there is also the option to display a 2D view on the topology of an arbitrary graph.

# 2D Graph Panel

The 2d Graph panel (*panels/2d/graph*) is a powerful tool for understanding and navigating the graph structure in smaller GroIMP models. 

![The 2d graph panel with the example graph and one collapsed node](src/main/resources/doc/res/graph.png)

In this visualization, every node in the graph is represented by rectangle with the Type and the ID and a simple color coding. If a subset of the graph is hidden (collapsed) the root of this subset is visualized by an oval. If a node has too many edge the subset is collapsed by default.   

The edges between the nodes are represented by small arrows. The visualization of the edge depends on the kind of edge (edgeBit). The straight green line represent a successor, the doted green line a branch and the purple line other edges. Therefore the pule edge also have labels, which indicate the edgeBit. 


## Navigation

The 2d graph can be navigated by using the buttons in the upper right corner, or with the mouse. With the mouse dragging is used for moving, scrolling for zooming and the "alt" key plus dragging will rotate the graph. 
The menu point View/redraw will always reset the navigation to no rotation and a zoom/position that fits the howl graph.

### Moving, Folding \& Editing

Clicking on a node or edge selects it and opens the attribute editor of this object.
After selecting a node it is possible manipulate its attributes in the editor or remove the node. (If the 2d view is opened in a new window the main window needs to be selected to remove the node). 

More over the arrangement of the nodes can be manipulated with the mouse via drag and drop. Yet the arrangement is reset if the redraw function is triggered. 
By double clicking on a node it is collapsed, meaning all the nodes below are hidden. This process does ignore custom edges and views the graph therefore as a tree. The collapsing of nodes can also be controlled by 'View/collapse all' and 'View/Expand all'. 

## Layout

The arrangement of the nodes and edges creates a not that trivial problem which can have different solutions based on the use case.
Therefore GroIMP comes with a set of layouts, which can be chosen and configured on 'Layout/Edit'. In most  cases the  [General Purpose Layout](#general-purpose-layout) is the best fit, therefore it is also the default one. 

### Project Layouts

Additionally it is possible to save Layout settings in the project, using the layout explorer panel (*panels/explorers/2d/layouts*). In this explorer new layout can be created on 'Object/new/...' and then configured.
Project layouts can then be selected similar to the other ones. 

## Graph properties

In the 2d view on the menu entry Graph/Edit the graph to display can be selected. **This feature has some issues!!** Therefore  the first entry should always be topology. Moreover in the source in the settings of the topology only project, meta and custom are useful:
- project shows the simulation graph of your model
- meta shows the meta graph of your simulation, in this graph objects added to the simulation are shown (e.g. the instances of the rgg files). 
- custom shows the graph of the below the root with the name given by customKey. This feature is only interesting if multiple roots are used in the simulation.



# General Purpose Layout

## How does it work ?

The \"General Purpose Layout\" combines a tree algorithm and an energy
algorithm backed by several optimization methods (mainly a
modified-Newton method and Simulated Annealing algorithm).The structure
of the graph is probed, and the graph is divided in linked components of
two types : sub-trees and loops. Each component is drawn independently
\-- sub-trees using the tree algorithm, loops using the energy
algorithm. The components are then combined together in a tree-like
fashion.

## What are the different options for ?

The \"Ideal Distance\" and \"Y-ideal Distance\" fields rule distances
between nodes. In the tree-parts of the graph, \"Ideal Distance\" and
\"Y-ideal Distance\" are respectively the horizontal and vertical
distances between nodes. In the \"loop-parts\" of the graph, the energy
algorithm tries to ensure a uniform distance equal to \"Ideal Distance\"
between two linked nodes.

The field \"Max Nb of Steps\" allows you to set the maximum number of
steps for the energy algorithm. One step corresponds to the modification
of the position of one node. When setting the maximal number of steps
you should remember that each node needs to be placed, and its position
optimized, and that this is all the more costly that the structure of
the loop is complex (high number of nodes / edges). If the number of
steps specified is less than the number of nodes to place, the \"Fast\"
option will be used instead.

The Fast option disables the use of the energy algorithm for drawing the
loops when ticked. The nodes belonging to a loop are then uniformly
placed on a circle, in an attempt to form a regular polygon. This option
significantly speeds-up the algorithm. There are several cases in which
you may want to choose this option :

- if you know that the loops in your graph are indeed forming regular polygons ;
- if you know that the loops in your graph are \"small\" (then this is likely to yield satisfactory results) ;
- if you want to draw a large graph, and do not care for an optimal drawing of each loop.

The \"Start Again\" option allows you to restart the drawing algorithm
from scratch without having to reset your model.

## Redraw and Modifications to the Graph

When new nodes are added, only the newly formed or modified loops will
be redrawn. If the new nodes can be placed without creating too many
disturbances to the loop, the overall configuration will stay the same.
However if the new nodes generate important modifications to the loop
structure, the layout may change radically. If a redraw is toggled with
no new nodes, the energy algorithm will try to improve the layout of
each loop (assuming the \"fast\" option is off ; if the \"fast\" option
is on, the result will be the same).

## What performances to expect ?

This depends radically on your options choice. You have to keep in mind
that the tree algorithm is fast and the energy algorithm slow. Drawing
trees, mixed tree-loop structures with loops up to 3 nodes (or with
bigger loops but the \"Fast\" option also on) will be fast \-- less than
1 second for 100,000 nodes \-- as no energy optimization is required.
When the energy algorithm is used, the computational cost depends on the
maximum number of steps allowed (field \"Max Nb of Steps\") and the
number of nodes (about one second for the AntSimulation graph \[60
nodes\] with Max Nb of Steps = 25,000).
