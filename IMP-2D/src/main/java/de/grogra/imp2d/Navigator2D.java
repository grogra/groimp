
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.imp2d;

import java.util.EventObject;
import java.awt.event.*;
import de.grogra.imp.*;
import de.grogra.imp.viewhandler.NavigatorEvent;
import de.grogra.imp.viewhandler.ViewEventHandler;

public class Navigator2D implements NavigatorEvent
{
	private final ViewEventHandler handler;
	private int lastX, lastY;
	private final boolean wheel;


	public Navigator2D (ViewEventHandler h, java.util.EventObject e)
	{
		handler = h;
		lastX = ((MouseEvent) e).getX ();
		lastY = ((MouseEvent) e).getY ();
		wheel = e instanceof MouseWheelEvent;
	}


	public void dispose ()
	{
	}


	public void eventOccured (EventObject e)
	{
		if (!(e instanceof MouseEvent))
		{
			return;
		}
		MouseEvent me = (MouseEvent) e;
		me.consume ();
		if (wheel)
		{
			if (!(me instanceof MouseWheelEvent))
			{
				handler.disposeEvent (me);
				return;
			}
//			IMP2D.move ((View2D) handler.getView (), 0,
//						-((MouseWheelEvent) me).getWheelRotation () << 5);
			
			IMP2D.zoom ((View2D) handler.getView (), 0, 
					((MouseWheelEvent) me).getWheelRotation () << 5);
			return;
		}
		switch (me.getID ())
		{
			case MouseEvent.MOUSE_RELEASED:
			case MouseEvent.MOUSE_MOVED:
				handler.disposeEvent (null);
				return;
			case MouseEvent.MOUSE_DRAGGED:
				int dx = me.getX () - lastX, dy = me.getY () - lastY;
				if ((dx == 0) && (dy == 0))
				{
					return;
				}
				lastX += dx;
				lastY += dy;
				View2D view = (View2D) handler.getView ();
				if (me.isMetaDown ())
				{
					IMP2D.zoom (view, dx, dy);
				}
				else if (me.isAltDown ())
				{
					IMP2D.rotate (view, dx, dy);
				}
				else
				{
					IMP2D.move (view, dx, dy);
				}
				break;
		}
	}

}
