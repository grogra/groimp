package de.grogra.imp2d;

import de.grogra.imp.PickList;
import de.grogra.imp.viewhandler.PickVisitor;

public interface PickVisitor2D extends PickVisitor{

	public void pick (View2D view, int x, int y, PickList list);
}
