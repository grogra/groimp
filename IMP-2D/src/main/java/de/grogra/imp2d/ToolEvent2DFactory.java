package de.grogra.imp2d;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.SwingUtilities;

import de.grogra.imp.viewhandler.DisposableViewEventFactory;
import de.grogra.imp.viewhandler.ToolEvent;
import de.grogra.imp.viewhandler.ViewEventHandler;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;

public class ToolEvent2DFactory extends ShareableBase implements DisposableViewEventFactory {
	
	//enh:sco SCOType
	
	@Override
	public ToolEvent createEvent
	(ViewEventHandler h, EventObject e) {
		return new ToolEvent2DImpl(h,e);
	}

	@Override
	public boolean isActivationEvent(ViewEventHandler h, EventObject e) {
		if (!(e instanceof MouseEvent))
		{
			return false;
		}
		MouseEvent me = (MouseEvent) e;
		return (SwingUtilities.isLeftMouseButton(me) && (me.getID () == MouseEvent.MOUSE_PRESSED )) && 
				h.isMouseOnNode(me);
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (ToolEvent2DFactory representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new ToolEvent2DFactory ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (ToolEvent2DFactory.class);
		$TYPE.validate ();
	}

//enh:end
}
