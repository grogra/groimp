function fix_path (path)
  return path:sub(24,path:len())
end

function string.starts(String,Start)
  return string.sub(String,1,string.len(Start))==Start
end

function Image (element)
  element.src = fix_path(element.src)
  return element
end

function Emph (emph)
  emphContent = pandoc.utils.stringify(emph.content)
  if(string.starts(emphContent,"panels"))
    then
    name = emphContent
    name= string.gsub(name,"explorers","objects")
    return pandoc.Link(emphContent, "command:showpanel?/ui/" .. name)
  elseif(string.starts(emphContent,"examples:"))
    then
      name =emphContent:match( "([^:]+)$" )
      return pandoc.Link(emphContent, "command:showexample?/" .. name)

  elseif(string.starts(emphContent,"preferences:"))
    then
      name =emphContent:match( "([^:]+)$" )
    return pandoc.Link(emphContent, "command:showoption?/" .. name)
  else
    return emph
  end
end
