package de.grogra.imp3d.objects;

import javax.vecmath.Vector3f;

public interface BoundedCloud extends Cloud {

	public float getMinimumX();
	public float getMinimumY();
	public float getMinimumZ();
	public float getMaximumX();
	public float getMaximumY();
	public float getMaximumZ();
	public Vector3f getCenter();
}
