package de.grogra.imp3d.objects;

import de.grogra.imp.objects.NodeContext;
import de.grogra.persistence.DisposableField;
import de.grogra.util.Disposable;

/**
 * Abstract object that represent the data structure of a cloud of objects.
 */
public interface Cloud extends CloudContext, NodeContext, Disposable, DisposableField {

	/**
	 * A point of a Cloud object
	 */
	public interface Point extends CloudContext {
		public Cloud getCloud();
		public void setCloud(Cloud c);
		public float[] toFloat();
	}
	
	/**
	 * To be part of the graph a cloud need to be added to a node. This return the 
	 * cloudNode associated to this cloud if any.
	 */
	public boolean isEmpty();
	
	public Point[] getPoints();

	/*
	 * Add a point to the pointcloud
	 */
	public void setPoints(Point[] p);
	public void addPoints(Point[] p);
	public void addPoint(Point p);
	
	/*
	 * Remove a point from the pointcloud
	 */
	public Point remove(Object i);
	public Point[] remove(Object[] i);
	public void removeAll();
	
	public Point pop();
	public Point peek();
	public void push(Point p);
	
	public int getNumberOfPoints();
	
	public Cloud createNew();
	
	public void dispose(boolean removeNode);
	/**
	 * A cloud is editable if it is possible to add and remove points. Being able to setPoint 
	 * does not make the cloud editable as is change either all or none of hte points.
	 */
	public boolean editable();
	
	/**
	 * Convenience methods as most points are/or can be represented as arrays of floats 
	 */
	public float[] pointsToFloat();
	public Point[] floatsToPoint(float[] f);
	
}
