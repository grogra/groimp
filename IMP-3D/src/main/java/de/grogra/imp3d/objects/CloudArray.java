package de.grogra.imp3d.objects;

import static java.lang.Math.max;
import static java.lang.Math.min;

import javax.vecmath.Vector3f;
import java.util.ArrayList;

import de.grogra.graph.impl.Node;
import de.grogra.imp.registry.EmbeddedFileObject;
import de.grogra.imp.registry.EmbeddedSharedObject;
import de.grogra.persistence.SCOType;
import de.grogra.persistence.SharedObjectProvider;
import de.grogra.util.MimeType;
import de.grogra.xl.lang.Aggregate;

public class CloudArray extends EmbeddedSharedObject implements BoundedCloud {
	
	//enh:sco SCOType
	
	transient private boolean disposed = false;
	transient float[] points;
	// enh:field hidden setmethod=setPoints getmethod=getPointsAsFloat

	transient protected Node cloudNode=null;
	transient private double minx, miny, minz, maxx, maxy, maxz;
	
	public CloudArray() {
		setPoints(new float[0]);
	}

	public CloudArray(float[] f) {
		setPoints(f);
	}
	
	public CloudArray(String name) {
		setPoints(new float[0]);
	}

	public CloudArray(Node cnode, float[] f) {
		setPoints(f);
		setNode(cnode);
	}
	
	public boolean editable() {
		return false;
	}

	public void setNode(Node cnode) {
		if (cloudNode != cnode) {
//			if ((EmbeddedFileObject)getProvider(false) != null){
//				((EmbeddedFileObject)getProvider(false)).changeNode(cnode);
//			}
		}
		this.cloudNode = cnode;
	}

	@Override
	public Node getNode() {
		return cloudNode;
	}

	@Override
	public boolean isEmpty() {
		return (points.length == 0);
	}

	@Override
	public Point[] getPoints() {
		return floatsToPoint(points);
	}

	@Override
	public void setPoints(Point[] p) {
		points = pointsToFloat(p);
		computeBounds();
	}

	public void setPoints(float[] p) {
		points = p;
		computeBounds();
	}
	
	/**
	 * Warning, this do NOT copy the array. Modify with care.
	 */
	public float[] getPointsAsFloat() {
		return points;
	}

	@Override
	public int getNumberOfPoints() {
		return points.length;
	}

	private void computeBounds() {
		if (points==null) { return; }
		// calculate bounding box of point cloud
		final int N = getPoints().length ;
		minx = miny = minz = Double.POSITIVE_INFINITY;
		maxx = maxy = maxz = Double.NEGATIVE_INFINITY;
		for (int i = 0; i < N; i++)
		{
			minx = min(minx, points[3*i+0]);
			miny = min(miny, points[3*i+1]);
			minz = min(minz, points[3*i+2]);
			maxx = max(maxx, points[3*i+0]);
			maxy = max(maxy, points[3*i+1]);
			maxz = max(maxz, points[3*i+2]);
		}
	}

	/**
	 * Update the bounds in the case where a new point is added
	 */
	private void updateBoundsAdd(Point p) {
		if (points==null) { return; }
		float[] f = p.toFloat();
		minx = min(minx, f[0]);
		miny = min(miny, f[1]);
		minz = min(minz, f[2]);
		maxx = max(maxx, f[0]);
		maxy = max(maxy, f[1]);
		maxz = max(maxz, f[2]);

	}

	/**
	 * Update the bounds in the case where a point is removed
	 */
	private void updateBoundsDel(Point p) {
		if (points==null) { return; }
		float[] f = p.toFloat();
		minx = min(minx, f[0]);
		miny = min(miny, f[1]);
		minz = min(minz, f[2]);
		maxx = max(maxx, f[0]);
		maxy = max(maxy, f[1]);
		maxz = max(maxz, f[2]);

		if (f[0] == minx || f[0] == maxx || f[1] == miny || f[1] == maxy || f[2] == minz || f[2] == maxz ) {
			computeBounds();
		}
	}

	@Override
	public float getMinimumX() {
		return (float) minx;
	}

	@Override
	public float getMinimumY() {
		return (float) miny;
	}

	@Override
	public float getMinimumZ() {
		return (float) minz;
	}

	@Override
	public float getMaximumX() {
		return (float) maxx;
	}

	@Override
	public float getMaximumY() {
		return (float) maxy;
	}

	@Override
	public float getMaximumZ() {
		return (float) maxz;
	}

	public void dispose(boolean removeNode) {
		if (disposed) {return;}
		disposed=true;
		removeAll();
		SharedObjectProvider s;
		if ((s = getProvider(false)) instanceof EmbeddedFileObject) {
			((EmbeddedFileObject)s).deactivate();
			((EmbeddedFileObject)s).remove();
		}
		if (removeNode) {
			Node n = getNode();
			if (n!=null) {
				n.removeAll(null);
			}
		}
	}
	
	@Override
	public void dispose() {
		dispose(true);
	}
	@Override
	public Cloud createNew() {
		return new CloudArray();
	}


	@Override
	public float[] pointsToFloat() {
		if (points!=null && points.length > 0) {
			return points;
		}
		return new float[0];
	}

	public float[] pointsToFloat(Point[] points) {
		if (points.length>0) {
			int dimension = points[0].toFloat().length; //  divisible by 3 
			float[] res = new float[points.length * dimension];
			int i = 0;
			for (Point p : points) {
				for (float f : p.toFloat()) {
					res[i]=f;
					i++;
				}
			}
			return res;
		} else {
			return new float[0];
		}
	}

	@Override
	public Point[] floatsToPoint(float[] f) {
//		if (f.length % 3 != 0) { return new ArrayPoint[0]; }
		int r = f.length % 3;
		ArrayPoint[] res = new ArrayPoint[(f.length-r)/3];
		int i = 0,j = 0;
		float[] point = new float[3];
		for (float p : f) {
			if (i == 2) {
				point[i] = p;
				res[j] = new ArrayPoint(point);
				j++;
				i=0;
				point = new float[3];
			} else {
				point[i] = p;
				i++;
			}
		}
		return res;
	}

	@Override
	public void addPoint(Point p) {
		throw new RuntimeException("Cannot add a specific points to a CloudArray");
	}
	@Override
	public void addPoints(Point[] p) {
		throw new RuntimeException("Cannot add a specific points to a CloudArray");
	}

	@Override
	public Point remove(Object i) {
		throw new RuntimeException("Cannot remove specific points from a CloudArray");
	}

	@Override
	public Point[] remove(Object[] i) {
		throw new RuntimeException("Cannot remove specific points from a CloudArray");
	}

	@Override
	public void removeAll() {
		points = new float[0];
	}

	@Override
	public Point peek() {
		return new ArrayPoint(new float[] 
				{points[points.length-3], points[points.length-2], points[points.length-1]});
	}

	//Probably should't be used on the CloudArray
	@Override
	public Point pop() {
		Point last = new ArrayPoint(new float[] 
				{points[points.length-3], points[points.length-2], points[points.length-1]});
		float[] newPoints = new float[points.length-3];
		System.arraycopy(points, 0, newPoints, 0, points.length-4);
		points = newPoints;
		updateBoundsDel(last);
		return last;
	}
	@Override
	public void push(Point p) {
		float[] newPoints = new float[points.length+3];
		System.arraycopy(points, 0, newPoints, 0, points.length+2);
		points = newPoints;
		updateBoundsDel(p);
	}

	@Override
	public Cloud getCloud() {
		return this;
	}

	/**
	 * Serialization management
	 */
	public MimeType getMimeType() {
		return new MimeType("model/x-grogra-cloud-array+xyz");
	}
	
	@Override
	public String getExtension() {
		return "xyz";
	}

	@Override
	public String getResourceDirectoryName() {
		return "pointclouds";
	}
	
	@Override
	public String getObjectType() {
		return Cloud.class.getName();
	}
	
	@Override
	public boolean hasContent() {
		if (getProvider(false) == null && !(pointsToFloat().length > 0)) {
			return false;
		}
		return true;
	}
	
	@Override
	public void disposeField() {
		SharedObjectProvider s;
		if ((s = getProvider(false)) instanceof EmbeddedFileObject) {
			((EmbeddedFileObject)s).deactivate();
			((EmbeddedFileObject)s).remove();
		}
	}
	
	public static CloudArray create(Aggregate a, Cloud.Point p) {
		if (a.initialize ())
		{
			a.aval1 = new ArrayList<Cloud.Point> ();
		}
		if (a.isFinished ())
		{		
			a.aval = new CloudArray();
			((CloudArray)a.aval).setPoints( ((ArrayList<Cloud.Point>)a.aval1).toArray(new Cloud.Point[0]) );
		}
		else
		{
			((ArrayList<Cloud.Point>) a.aval1).add (p);
		}
		return null;
	}
	/**
	 * Add Object to graph
	 */
	@Override
	public Node produceNode() {
		return ( getNode()!=null)? getNode() : new PointCloudImpl(this);
	}

	@Override
	/**
	 * Returns the center position of this point cloud as a point object. This
	 * method does not return the point with the most-centric position, it
	 * returns a position with the average position of all points in the point
	 * cloud instead.
	 *
	 * @return The center position of this point cloud as a point object
	 */
	public Vector3f getCenter() {
		float centerX = this.getCenterX();
		float centerY = this.getCenterY();
		float centerZ = this.getCenterZ();
		return new Vector3f(centerX, centerY, centerZ);
	}
	/**
	 * Returns the center x position of this point cloud.
	 *
	 * @return The center x position of this point cloud
	 */
	public float getCenterX() {
		return this.getMinimumX() + (this.getMaximumX() - this.getMinimumX()) / 2;
	}
	/**
	 * Returns the center y position of this point cloud.
	 *
	 * @return The center y position of this point cloud
	 */
	public float getCenterY() {
		return this.getMinimumY() + (this.getMaximumY() - this.getMinimumY()) / 2;
	}
	/**
	 * Returns the center z position of this point cloud.
	 *
	 * @return The center z position of this point cloud
	 */
	public float getCenterZ() {
		return this.getMinimumZ() + (this.getMaximumZ() - this.getMinimumZ()) / 2;
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field points$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (CloudArray representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 1;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((CloudArray) o).setPoints ((float[]) value);
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((CloudArray) o).getPointsAsFloat ();
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new CloudArray ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (CloudArray.class);
		points$FIELD = Type._addManagedField ($TYPE, "points", Type.Field.TRANSIENT  | Type.Field.SCO | Type.Field.HIDDEN, de.grogra.reflect.ClassAdapter.wrap (float[].class), null, Type.SUPER_FIELD_COUNT + 0);
		$TYPE.validate ();
	}

//enh:end
}
