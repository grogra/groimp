package de.grogra.imp.fswatcher;

import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

import de.grogra.pf.ui.Context;

public class WatcherService {

    private final WatchService watcher;
    private final Map<WatchKey, Path> keys;
    private boolean trace;
    public Context context;

    WatcherService(Path dir, Context ctx) throws IOException {
        watcher = FileSystems.getDefault().newWatchService();
        keys = new HashMap<>();
        context = ctx;

        registerAll(dir);

        trace = true;
    }
    
    
    private void registerAll(final Path start) throws IOException {
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }
    
    
    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, 
        		StandardWatchEventKinds.ENTRY_DELETE, 
        		StandardWatchEventKinds.ENTRY_MODIFY);
        if (trace) {
            Path prev = keys.get(key);
            if (null == prev) {
            	System.err.println("Register path: "+ dir);
            } else {
                if (!dir.equals(prev)) {
                	System.err.println("Updated path: "+ prev + " -> " + dir);
                }
            }
        }
        keys.put(key, dir);
    }
    
    
    public void close() throws IOException {
    	watcher.close();
    }

    boolean watch() throws IOException, InterruptedException {
        WatchKey key;
        try {
            key = watcher.take();
        } catch (InterruptedException exc) {
            return false;
        } catch (ClosedWatchServiceException exc) {
            return false;
        }
        
        Path dir = keys.get(key);
        if (null == dir) {
        	System.err.println("WatchKey is not recognized!");
            return false;
        }

        // forEach?
        for (WatchEvent event: key.pollEvents()) {
            WatchEvent.Kind kind = event.kind();
            if (StandardWatchEventKinds.OVERFLOW == kind) {
            	// should handle this case? Could happen if there are a lot of inputs from the device 
                continue;
            }

            WatchEvent<Path> ev = (WatchEvent<Path>) event;
            Path name = ev.context();
            Path child = dir.resolve(name);
                        
            if (StandardWatchEventKinds.ENTRY_CREATE == kind
            		&& child.toFile().isDirectory()) {
            	registerAll(child);
            }
            
            WatchEventResolver.resolveEvent(ev, child, context);
        }

        boolean valid = key.reset();
        if (!valid) {
            keys.remove(key);

            if (keys.isEmpty()) {
                return false;
            }
        }

        return true;
    }
}