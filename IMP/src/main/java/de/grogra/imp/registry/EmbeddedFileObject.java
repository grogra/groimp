package de.grogra.imp.registry;

import java.io.IOException;

import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSourceImpl;
import de.grogra.pf.io.VirtualFileWriterSource;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Workbench;
import de.grogra.pf.ui.registry.FileObjectItem;
import de.grogra.util.MimeType;

/**
 * Shared object provider for object that are created in the project graph.
 */
public class EmbeddedFileObject extends FileObjectItem {
		
	protected EmbeddedFileObject() {
		super();
	}
	public EmbeddedFileObject (String systemId, MimeType mimeType, Object object,
			   String type) {
		super(systemId, mimeType, object, type);
	}
	
	/**
	 * Write the object in the file for update.
	 */
	public void writeUpdateFile() {
		Registry r = getRegistry();
		Object file = r.getProjectFile(getSystemId());
		
		FilterSource src = new ObjectSourceImpl(getObject(), getSystemId(), getMimeType(), 
				r.getRootRegistry(), null);
		FilterSource fs = IO.createPipeline(src, new IOFlavor(getMimeType(), IOFlavor.VFILE_WRITER, null));

		try {
			((VirtualFileWriterSource) fs).write(r.getFileSystem() ,src);
		} catch (IOException e) {
			Workbench.current().logGUIInfo
			(IO.I18N.msg ("openfile.failed", getSystemId()));
		}
	}
	
		
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new EmbeddedFileObject ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new EmbeddedFileObject ();
	}

//enh:end

}
