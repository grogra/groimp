package de.grogra.imp.viewhandler;

import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.EventObject;

import javax.swing.SwingUtilities;

import de.grogra.persistence.ShareableBase;

/**
 * The <code>NavigatorEventFactory</code> is the factory for the <code>NavigatorEvent</code>.
 * 
 * It defines the default activation events that trigger the navigation mode.
 * The activation events are: 
 * <ul>
 *   <li>Right mouse button pressed.</li>
 *   <li>Wheel used.</li>
 *   <li>Middle button pressed.</li>
 *   <li>Left mouse button dragged over empty space.</li>
 * </ul>
 * 
 * The activation events can be overrided to change the activation.

 */
public abstract class NavigatorEventFactory extends ShareableBase implements DisposableViewEventFactory
{
	public boolean isActivationEvent (ViewEventHandler h, EventObject e)
	{
		if (!(e instanceof MouseEvent))
		{
			return false;
		}
		MouseEvent me = (MouseEvent) e;
		return (SwingUtilities.isRightMouseButton(me) && (me.getID () == MouseEvent.MOUSE_PRESSED)) || 
			   (SwingUtilities.isMiddleMouseButton(me) && (me.getID () == MouseEvent.MOUSE_PRESSED)) ||
			   (e instanceof MouseWheelEvent) ||
			   (!me.isControlDown() && !me.isAltDown() && !me.isShiftDown() && SwingUtilities.isLeftMouseButton(me) && (me.getID () == MouseEvent.MOUSE_DRAGGED));
	}
}
