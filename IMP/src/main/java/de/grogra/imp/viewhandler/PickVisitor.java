package de.grogra.imp.viewhandler;

/**
 * Needs to be implemented by Visitor that enables picking in a view. See PickVisitor2D and PickVisitor3D
 */
public interface PickVisitor {

}
