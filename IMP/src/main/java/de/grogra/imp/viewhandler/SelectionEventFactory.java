package de.grogra.imp.viewhandler;

import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.SwingUtilities;

import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;

/** 
 * It defines the activation events that trigger the selection mode.
 * The default activation events are: 
 * <ul>
 *   <li>Left mouse button pressed over an object in the scene.</li>
 * </ul>
*/
public class SelectionEventFactory extends ShareableBase implements DisposableViewEventFactory{
	
	//enh:sco SCOType
	
	@Override
	public SelectionEvent createEvent
	(ViewEventHandler h, EventObject e) {
		return new SelectionEventImpl(h,e);
	}

	public boolean isActivationEvent (ViewEventHandler h, EventObject e)
	{
		if (!(e instanceof MouseEvent))
		{
			return false;
		}
		MouseEvent me = (MouseEvent) e;
		return ((!me.isAltDown() && me.getID () ==MouseEvent.MOUSE_RELEASED) );
	}
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;


	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (SelectionEventFactory representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}


		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		public Object newInstance ()
		{
			return new SelectionEventFactory ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (SelectionEventFactory.class);
		$TYPE.validate ();
	}

//enh:end
}
