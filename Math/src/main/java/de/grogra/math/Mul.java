package de.grogra.math;

import de.grogra.persistence.SCOType;
import de.grogra.persistence.ShareableBase;
import de.grogra.xl.lang.FloatToFloat;

public class Mul extends ShareableBase implements FloatToFloat
{
	//enh:sco SCOType


	FloatToFloat second;
	//enh:field

	public float evaluateFloat (float v)
	{
		if (second != null)
		{
			v *= second.evaluateFloat (1);
		}
		return v;
	}
	
//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final Type $TYPE;

	public static final Type.Field second$FIELD;

	public static class Type extends SCOType
	{
		public Type (Class c, de.grogra.persistence.SCOType supertype)
		{
			super (c, supertype);
		}

		public Type (Mul representative, de.grogra.persistence.SCOType supertype)
		{
			super (representative, supertype);
		}

		Type (Class c)
		{
			super (c, SCOType.$TYPE);
		}

		private static final int SUPER_FIELD_COUNT = SCOType.FIELD_COUNT;
		protected static final int FIELD_COUNT = SCOType.FIELD_COUNT + 1;

		static Field _addManagedField (Type t, String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			return t.addManagedField (name, modifiers, type, componentType, id);
		}

		@Override
		protected void setObject (Object o, int id, Object value)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					((Mul) o).second = (FloatToFloat) value;
					return;
			}
			super.setObject (o, id, value);
		}

		@Override
		protected Object getObject (Object o, int id)
		{
			switch (id)
			{
				case Type.SUPER_FIELD_COUNT + 0:
					return ((Mul) o).second;
			}
			return super.getObject (o, id);
		}

		@Override
		public Object newInstance ()
		{
			return new Mul ();
		}

	}

	public de.grogra.persistence.ManageableType getManageableType ()
	{
		return $TYPE;
	}


	static
	{
		$TYPE = new Type (Mul.class);
		second$FIELD = Type._addManagedField ($TYPE, "second", 0 | Type.Field.SCO, de.grogra.reflect.ClassAdapter.wrap (FloatToFloat.class), null, Type.SUPER_FIELD_COUNT + 0);
		$TYPE.validate ();
	}

//enh:end
}
