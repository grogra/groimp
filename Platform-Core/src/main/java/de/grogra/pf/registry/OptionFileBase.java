package de.grogra.pf.registry;

public abstract class OptionFileBase extends Item {

	private OptionFileBase() {
		this(null);
	}
	public OptionFileBase(String key) {
		super(key);
	}

	public abstract String getOptionStringValue(String key);
	public abstract String getOptionStringValue(Option opt);
	public abstract void setOptionStringValue(Option opt, String value);
	
}
