
/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.ui.swing;

import java.io.*;
import java.awt.*;
import java.awt.event.*;
import java.util.MissingResourceException;
import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import javax.swing.undo.*;

import javax.swing.event.*;
import de.grogra.util.*;
import de.grogra.icon.*;
import de.grogra.pf.datatransfer.UITransferHandler;
import de.grogra.pf.io.*;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.*;
import de.grogra.pf.ui.Window;
import de.grogra.pf.ui.autocomplete.AutoCompletableTextArea;
import de.grogra.pf.ui.autocomplete.impl.AbstractAutoCompletor;
import de.grogra.pf.ui.autocomplete.impl.CompletionProvider;
import de.grogra.pf.ui.registry.PanelFactory;

public class TextEditorSupport extends AutoCompletableTextEditorSupport
implements ChangeListener, ModifiableMap.Producer
{
	protected class Doc extends javax.swing.text.PlainDocument implements Runnable, FileDocument
	{
		final String systemId;
		MimeType mimeType;
		final Workbench wb;
		final boolean editable;
		boolean modified;

		Doc (Workbench wb, String systemId)
		{
			this.systemId = systemId;
			this.wb = wb;
			FileSource file = null;
			mimeType = MimeType.TEXT_PLAIN;
			try
			{
				file = FileSource.createFileSource
						(systemId, IO.getMimeType (systemId), wb, null);
				this.mimeType = file.getFlavor ().getMimeType ();
				insertString (0, file.readContent ().toString (), null);
			}
			catch (javax.swing.text.BadLocationException e)
			{
				wb.logInfo (null, e);
			}
			catch (Exception e)
			{
				//TODO: Workaround for saved projects created with jEdit
				// TextEditor is then called with systemId=Untitled 
				//				wb.logGUIInfo (IO.I18N.msg ("readfile.failed", systemId), e);
			}
			putProperty (TitleProperty, IO.toPath (systemId));
			editable = (file != null) && !file.isReadOnly ();
			modified = false;
		}


		public void save ()
		{
			render (this);
		}


		public void run ()
		{
			FileSource file = FileSource.createFileSource
					(systemId, mimeType, wb, null);
			Writer out = null;
			try
			{
				out = file.getWriter (false);
				out.write (getText (0, getLength ()));
				out.flush ();
				modified = false;
			}
			catch (IOException e)
			{
				wb.logGUIInfo
				(IO.I18N.msg ("writefile.failed", file.getSystemId ()), e);
			}
			catch (javax.swing.text.BadLocationException e)
			{
				wb.logInfo (null, e);
			}
			finally
			{
				if (out != null)
				{
					try
					{
						out.close ();
					}
					catch (IOException e)
					{
						wb.logGUIInfo
						(IO.I18N.msg ("closefile.failed", file.getSystemId ()), e);
					}
				}
			}
		}


		@Override
		protected void postRemoveUpdate (DefaultDocumentEvent e)
		{
			super.postRemoveUpdate (e);
			modified = true;
		}


		@Override
		protected void insertUpdate (DefaultDocumentEvent e,
				javax.swing.text.AttributeSet a)
		{
			super.insertUpdate (e, a);
			modified = true;
		}


		@Override
		protected void fireRemoveUpdate (DocumentEvent e)
		{
			super.fireRemoveUpdate (e);
			modified = true;
		}


		@Override
		protected void fireInsertUpdate (DocumentEvent e)
		{
			super.fireInsertUpdate (e);
			modified = true;
		}


		@Override
		protected void fireChangedUpdate (DocumentEvent e)
		{
			super.fireChangedUpdate (e);
			modified = true;
		}


		@Override
		public boolean isModified() {
			return modified;
		}


		@Override
		public boolean isEditable() {
			return editable;
		}


		@Override
		public String getSystemId() {
			return systemId;
		}


		@Override
		public MimeType getMimeType() {
			return mimeType;
		}

	}

	transient UITransferHandler transferhandler;
	transient java.util.List datahandlers;

	private static final String UNDO_MANAGER
	= "de.grogra.pf.ui.swing.TextEditorSupport.UndoManager";

	private static final String HANDLER
	= "de.grogra.pf.ui.swing.TextEditorSupport.Handler";

	private static final String DOCUMENT
	= "de.grogra.pf.ui.swing.TextEditorSupport.Document";

	private static final String EDITOR
	= "de.grogra.pf.ui.swing.TextEditorSupport.Editor";

	private static final String[] KEYS
	= {Action.NAME, Action.SHORT_DESCRIPTION, Action.ACCELERATOR_KEY};


	private class ActionImpl extends AbstractAction
	{
		ActionImpl (String name)
		{
			super (name);
			setEnabled (false);
			putValue (ACTION_COMMAND_KEY, name);
			I18NBundle b = UI.I18N;
			Object o;
			try
			{
				o = b.getObject ("text." + name + '.' + Described.ICON);
				if (o instanceof IconSource)
				{
					putValue (SMALL_ICON, IconAdapter.create
							((IconSource) o, SwingToolkit.MENU_ICON_SIZE));
				}
			}
			catch (MissingResourceException e)
			{
			}
			for (int i = 0; i < KEYS.length; i++)
			{
				putValue (KEYS[i],
						b.getStringOrNull ("text." + name + '.' + KEYS[i]));
			}
		}

		public void actionPerformed (ActionEvent e)
		{
			TextEditorSupport.this.actionPerformed (e);
		}
	}


	protected ActionImpl undoAction, redoAction, saveAction, cutAction, copyAction,
	pasteAction, closeAction;
	protected Object doUndo, doRedo, doSave;
	JTabbedPane tab;

	public TextEditorSupport ()
	{
		super (new SwingPanel (null));
		Container c = ((SwingPanel) getComponent ()).getContentPane ();
		c.setLayout (new BorderLayout ());

		JToolBar tb = new JToolBar ();
		tb.setFloatable (false);
		tb.setRollover (true);
		SwingToolkit.setDisabledIcon
		(tb.add (saveAction = new ActionImpl ("save")));
		SwingToolkit.setDisabledIcon
		(tb.add (undoAction = new ActionImpl ("undo")));
		SwingToolkit.setDisabledIcon
		(tb.add (redoAction = new ActionImpl ("redo")));
		SwingToolkit.setDisabledIcon
		(tb.add (cutAction = new ActionImpl ("cut")));
		SwingToolkit.setDisabledIcon
		(tb.add (copyAction = new ActionImpl ("copy")));
		SwingToolkit.setDisabledIcon
		(tb.add (pasteAction = new ActionImpl ("paste")));
		SwingToolkit.setDisabledIcon
		(tb.add (closeAction = new ActionImpl ("close")));
		c.add (tb, BorderLayout.NORTH);

		doUndo = new Object();
		doRedo = new Object();
		doSave = new Object();

		tab = new JTabbedPane (JTabbedPane.BOTTOM, JTabbedPane.SCROLL_TAB_LAYOUT);
		c.add (tab, BorderLayout.CENTER);
		tab.addChangeListener (this);
		mapProducer = this;
	}


	@Override
	protected void configure (Map params)
	{
		super.configure (params);
		setupTransferHandler(params);
		String[] docs = UI.getDocuments (params);
		for (int j = 0; j < docs.length; j++)
		{
			openDocument (docs[j], null);
		}
		String s = (String) params.get ("selected", null);
		if (s != null)
		{
			openDocument (s, null);
		}
		
	}

	protected void setupTransferHandler(Map params) {
		Object o = params.get(PanelFactory.TRANSFER_HANDLER, null);
		if (o instanceof UITransferHandler) {
			transferhandler = (UITransferHandler)o;
		}
		o = params.get(PanelFactory.DATA_HANDLER, null);
		if (o instanceof java.util.List) {
			datahandlers = (java.util.List)o;
		}
	}

	protected void openDocumentSync (String systemId, String ref)
	{
		JTextArea editor;
		getEditor:
		{
			for (int i = 0; i < tab.getTabCount (); i++)
			{
				if (systemId.equals (((FileDocument) ((JComponent) tab.getComponentAt (i))
						.getClientProperty (DOCUMENT)).getSystemId()))
				{
					tab.setSelectedIndex (i);
					editor = (JTextArea) ((JComponent) tab.getComponentAt (i))
							.getClientProperty (EDITOR);
					break getEditor;
				}
			}

			
			final FileDocument doc = createDoc(getWorkbench (), systemId);
			editor = createEditor(doc);

			JScrollPane sp = (JScrollPane)createScrollPanel(editor);
			final UndoManager m = new UndoManager ();
			sp.putClientProperty (UNDO_MANAGER, m);
			sp.putClientProperty (DOCUMENT, doc);
			sp.putClientProperty (EDITOR, editor);
			final JTextArea ed = editor;
						
			class Handler implements UndoableEditListener, Disposable,
			DocumentListener
			{
				public void undoableEditHappened (UndoableEditEvent e)
				{
					m.addEdit (e.getEdit ());
					update (ed, m, doc);
				}

				public void dispose ()
				{
					doc.removeUndoableEditListener (this);
					doc.removeDocumentListener (this);
					try
					{
						//						ed.setDocument (null);
					}
					catch (NullPointerException e)
					{
					}
				}

				public void changedUpdate (DocumentEvent e)
				{
					update (ed, m, (FileDocument) e.getDocument ());
				}

				public void insertUpdate (DocumentEvent e)
				{
					changedUpdate (e);
				}

				public void removeUpdate (DocumentEvent e)
				{
					changedUpdate (e);
				}
			}

			Handler h = new Handler ();				
			sp.putClientProperty (HANDLER, h);
			doc.addUndoableEditListener (h);
			doc.addDocumentListener (h);
			tab.addTab (String.valueOf
					(doc.getProperty (Document.TitleProperty)),
					IconAdapter.create
					(UI.getIcon (doc.getSystemId(),
							doc.getMimeType().getMediaType (),
							null, this, true),
							SwingToolkit.MENU_ICON_SIZE), sp);
			tab.setSelectedComponent (sp);
			update (editor, m, doc);
		}
		editor.requestFocus ();
		installAutoCompletor(autoCompl);
		configureEditor(editor);
		Point[] range = UI.parsePlainTextRange (ref);
		if (range != null)
		{
			try
			{
				Point p = range[0];
				int start = editor.getLineStartOffset (p.y);
				if (p.x >= 0)
				{
					start += p.x;
				}
				p = range[1];
				if (p == null)
				{
					editor.setCaretPosition (start);
				}
				else
				{
					editor.select (start, (p.x >= 0)
							? editor.getLineStartOffset (p.y) + p.x
									: editor.getLineEndOffset (p.y));
				}
			}
			catch (BadLocationException e)
			{
			}
		}
	}
	
	protected JComponent createScrollPanel(JTextArea te) {
		JScrollPane sp = new JScrollPane();
		sp.getViewport ().add (te);
		return sp;

	}
	
	protected FileDocument createDoc(Workbench wb, String id) {
		return new Doc(wb, id);
	}
	
	protected JTextArea createEditor(FileDocument doc) {
		AutoCompletableTextEditor editor = new AutoCompletableTextEditor (doc);
		editor.setEditable (doc.isEditable());

		editor.setTabSize (4);
		editor.setFont (new Font ("Monospaced", Font.PLAIN, 12));
		editor.setDragEnabled (true);

		//hotkeys
		editor.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK), doSave);
		editor.getActionMap().put(doSave, saveAction);
		editor.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, Event.CTRL_MASK), doUndo);
		editor.getActionMap().put(doUndo, undoAction);
		editor.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, Event.CTRL_MASK), doRedo);
		editor.getActionMap().put(doRedo, redoAction);
		
		UIToolkit.get(this).setTransferHandler(editor, transferhandler);
		UIToolkit.get(this).setDataHandlers(editor, (java.util.List<Item>) datahandlers);
		return editor;
	}

	protected void configureEditor(JTextArea editor) {
	}

	private static final int OPEN_DOC = MIN_UNUSED_ACTION;
	private static final int CLOSE_DOC = MIN_UNUSED_ACTION + 1;
	private static final int UPDATE = MIN_UNUSED_ACTION + 2;

	@Override
	public Object run (int action, int iarg, Object arg, Object arg2)
	{
		switch (action)
		{
		case OPEN_DOC:
		{
			openDocumentSync ((String) arg, (String) arg2);
			break;
		}
		case CLOSE_DOC:
		{
			for (int i = 0; i < tab.getTabCount (); i++)
			{
				if (((JComponent) tab.getComponentAt (i))
						.getClientProperty (DOCUMENT) == arg)
				{
					Disposable d = (Disposable)
							((JComponent) tab.getComponentAt (i))
							.getClientProperty (HANDLER);
					tab.removeTabAt (i);
					d.dispose ();
					break;
				}
			}
			break;
		}
		case UPDATE:
		{
			update ((JComponent) arg);
			break;
		}
		default:
			return super.run (action, iarg, arg, arg2);
		}
		return null;
	}


	public void openDocument (String doc, String ref)
	{
		sync.invokeAndWait (OPEN_DOC, 0, doc, ref);
	}


	void closeDocument (FileDocument doc)
	{
		sync.invokeAndWait (CLOSE_DOC, doc);
	}

	/*
	void resetUndoManager ()
	{
		undo.discardAllEdits ();
		update ();
	}
	 */  

	void update (JTextArea editor, UndoManager m, FileDocument doc)
	{
		for (int i = 0; i < tab.getTabCount (); i++)
		{
			if (((JComponent) tab.getComponentAt (i))
					.getClientProperty (DOCUMENT) == doc)
			{
				String s = String.valueOf
						(doc.getProperty (Document.TitleProperty));
				if (doc.isModified())
				{
					s = "* " + s + " *";
				}
				if (!s.equals (tab.getTitleAt (i)))
				{
					tab.setTitleAt (i, s);
				}
				break;
			}
		}
		undoAction.setEnabled ((m != null) && m.canUndo ());
		redoAction.setEnabled ((m != null) && m.canRedo ());
		saveAction.setEnabled ((doc != null) && doc.isModified());
		cutAction.setEnabled (editor != null);
		copyAction.setEnabled (editor != null);
		pasteAction.setEnabled (editor != null);
		closeAction.setEnabled (editor != null);
	}


	void update (JComponent c)
	{
		if (c == null)
		{
			update (null, null, null);
		}
		else
		{
			update ((JTextArea) c.getClientProperty (EDITOR),
					(UndoManager) c.getClientProperty (UNDO_MANAGER),
					(FileDocument) c.getClientProperty (DOCUMENT));
			installAutoCompletor(autoCompl);
		}
	}


	void actionPerformed (ActionEvent a)
	{
		final JComponent c = (JComponent) tab.getSelectedComponent ();
		if (c == null)
		{
			return;
		}
		UndoManager m = (UndoManager) c.getClientProperty (UNDO_MANAGER);
		JTextArea editor = (JTextArea) c.getClientProperty (EDITOR);
		FileDocument doc = (FileDocument) c.getClientProperty (DOCUMENT);
		String cmd = a.getActionCommand ();
		if ("undo".equals (cmd))
		{
			try
			{
				m.undo ();
			}
			catch (CannotUndoException e)
			{
				e.printStackTrace ();
			}
			update (editor, m, doc);
		}
		else if ("redo".equals (cmd))
		{
			try
			{
				m.redo ();
			}
			catch (CannotRedoException e)
			{
				e.printStackTrace ();
			}
			update (editor, m, doc);
		}
		else if ("save".equals (cmd))
		{
			getWorkbench ().getJobManager ().execute
			(new Command ()
			{
				public String getCommandName ()
				{
					return null;
				}

				public void run (Object info, Context context)
				{
					((FileDocument) info).save ();
					sync.invokeAndWait (UPDATE, c);
				}
			},
			doc, this, JobManager.ACTION_FLAGS);
		}
		else if ("cut".equals (cmd))
		{
			editor.cut ();
		}
		else if ("copy".equals (cmd))
		{
			editor.copy ();
		}
		else if ("paste".equals (cmd))
		{
			editor.paste ();
		}
		else if ("close".equals (cmd))
		{
			getWorkbench ().getJobManager ().execute
			(new Command ()
			{
				public void run (Object info, Context context)
				{
					if (canClose ((FileDocument) info))
					{
						closeDocument ((FileDocument) info);
					}
				}

				public String getCommandName ()
				{
					return null;
				}
			},
			doc, this, JobManager.ACTION_FLAGS);
		}
	}


	public void stateChanged (ChangeEvent e)
	{
		update ((JComponent) tab.getSelectedComponent ());
	}


	@Override
	protected void disposeImpl ()
	{
		for (int i = 0; i < tab.getTabCount (); i++)
		{
			((Disposable) ((JComponent) tab.getComponentAt (i))
					.getClientProperty (HANDLER)).dispose ();
		}
		super.disposeImpl ();
	}


	@Override
	public void checkClose (Runnable ok)
	{
		executeCheckClose (ok);
	}


	boolean canClose (FileDocument d)
	{
		return !d.isModified()
				|| (getWindow ().showDialog
						(UI.I18N.msg ("text.savequestion.title"),
								UI.I18N.msg ("text.savequestion.msg",
										d.getProperty (Document.TitleProperty)),
								Window.QUESTION_MESSAGE) == Window.YES_OK_RESULT);
	}


	@Override
	public void checkClose (Command ok)
	{
		for (int i = 0; i < tab.getTabCount (); i++)
		{
			FileDocument d = (FileDocument) ((JComponent) tab.getComponentAt (i))
					.getClientProperty (DOCUMENT);
			if (!canClose (d))
			{
				return;
			}
		}
		ok.run (null, this);
	}


	public String[] getDocuments ()
	{
		int n = tab.getTabCount ();
		String[] d = new String[n];
		for (int i = 0; i < n; i++)
		{
			d[i] = ((FileDocument) ((JComponent) tab.getComponentAt (i))
					.getClientProperty (DOCUMENT)).getSystemId();
		}
		return d;
	}


	public void addMappings (ModifiableMap out)
	{
		UI.putDocuments (this, out);
		Component c = tab.getSelectedComponent ();
		if (c != null)
		{
			out.put ("selected",
					((FileDocument) ((JComponent) c).getClientProperty (DOCUMENT))
					.getSystemId());
		}
	}


	@Override
	public void closeDocument(String systemId) {
		sync.invokeAndWait (CLOSE_DOC, 0, systemId, null);
	}

	// with JTextArea and their extended classes the autocomplete method is binded to 
	// a key action. So there is no need to implement an additional one here
	public void autocomplete() {}
	
	public String getAlreadyEnteredText(CompletionProvider provider) {
		final JComponent c = (JComponent) tab.getSelectedComponent ();
		if (c!=null) {
			JTextArea editor = (JTextArea) c.getClientProperty (EDITOR);
			if (editor instanceof AutoCompletableTextEditor) {
				return ((AutoCompletableTextEditor)editor).getAlreadyEnteredText(provider);
			}
		}
		return "";

	}
	
	@Override
	public void installAutoCompletor(AbstractAutoCompletor ac) {
		if (ac==null ) {return;}
		final JComponent c = (JComponent) tab.getSelectedComponent ();
		if (c!=null) {
			JTextArea editor = (JTextArea) c.getClientProperty (EDITOR);
			if (editor instanceof AutoCompletableTextArea) {
				((AutoCompletableTextArea)editor).initCompletionProvider();
				ac.install((AutoCompletableTextArea)editor, getRegistry());
			}
		}
	}


	@Override
	public void uninstallAutoCompletor() {
		if (autoCompl!=null) {
			autoCompl.uninstall();
		}
		// remove the ac from the editor of each doc? 
	}

//
//	@Override
//	public void addAtXY(int x, int y, Object o) {
//		if (o instanceof String) {
//			final JComponent c = (JComponent) tab.getSelectedComponent ();
//			JTextArea editor = (JTextArea)c.getClientProperty (EDITOR);
//			editor.insert((String)o,editor.getCaretPosition());
//		}
//	}
}
