package de.grogra.pf.ui.swing;

import java.util.prefs.Preferences;

import javax.swing.DefaultListModel;

import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.Option;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.expr.Expression;
import de.grogra.pf.ui.TextEditor;
import de.grogra.pf.ui.edit.EnumerationEditor;
import de.grogra.util.StringMap;

/**
 * Hook to load the available text editor and create an option list for them.
 */
public class TextEditors {

	DefaultListModel<String> list = new DefaultListModel ();
	final static StringMap allTextEditors = new StringMap ();

	public TextEditors (Registry reg)
	{
		Item item = Item.resolveItem(reg.getRootRegistry(), "/ui/toolkits/swing/texteditors") ;
		if (item == null) { // add the default?
			return;
		}
		Item opt = Item.resolveItem(reg.getRootRegistry(), "/ui/options") ;
		opt = Option.get (opt, "texteditor");
		if (item != null)
		{
			Item e = (Item) opt.getBranch ();
			if (e instanceof EnumerationEditor)
			{
				for (Item i = (Item) item.getBranch (); i != null;
						i = (Item) i.getSuccessor ())
				{
					if (i instanceof Expression)
					{
						allTextEditors.put(i.getName(), i);
					}
				}
				for (int i = 0; i < allTextEditors.size (); i++)
				{
					list.addElement (allTextEditors.getKeyAt (i));
				}
				((EnumerationEditor) e).setList (list);
			}
		}
		String curName = null;
		Preferences prefs0 = Preferences.userRoot().node(this.getClass().getName());
		if (prefs0!=null) {
			Preferences  prefs1 = prefs0.node("/de/grogra/options/ui/options");
			if(prefs1!=null) {
				curName = prefs1.get("texteditor", null);
			}
		}
		if (curName != null && allTextEditors.containsKey(curName)) {
			((Option)opt).setOptionValue(list.getElementAt(allTextEditors.findIndex(curName)));;
		} else {curName = null;}
		if ( (curName==null || curName.isEmpty() ) && list.size() > 0) {
			if (list.contains("default")) {
				((Option)opt).setOptionValue(list.getElementAt(list.indexOf("default")));
			} else {
				((Option)opt).setOptionValue(list.getElementAt(0));
			}
		}
	}	

	public static Item getTextEditor(String name) {
		return (Item) allTextEditors.get(name);
	}
}
