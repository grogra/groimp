package de.grogra.pf.ui.swing.datatransfer;

import de.grogra.pf.datatransfer.UITransferHandler;
import de.grogra.pf.ui.registry.UITransferHandlerFactory;

public class ButtonTransferHandlerFactory extends UITransferHandlerFactory {

	@Override
	public UITransferHandler createHandler() {
		return new ButtonTransferHandler();
	} 
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new ButtonTransferHandlerFactory ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new ButtonTransferHandlerFactory ();
	}

//enh:end

}
