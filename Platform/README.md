# Platform

Platform is the basic plug-in of GroIMP and is referenced (directly or
indirectly) by most other plug-ins. It contains an abstraction of a graphical
user interface around the interface Workbench.

It includes support of the file types :
- ".gsz" 
- ".gs"
- ".jar"
- ".class"
- ".txt"
- ".ini"
- ".fun"
- ".properties"
- ".sci"
- ".html"
- ".xml"
- ".grahpml"
- ".xls"
- ".ps"
- ".csv"
- ".pdf"

