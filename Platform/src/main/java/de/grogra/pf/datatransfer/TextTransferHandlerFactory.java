package de.grogra.pf.datatransfer;

import de.grogra.pf.ui.registry.UITransferHandlerFactory;

public class TextTransferHandlerFactory extends UITransferHandlerFactory {

	@Override
	public UITransferHandler createHandler() {
		return new TextTransferHandler();
	} 
	
	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new TextTransferHandlerFactory ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new TextTransferHandlerFactory ();
	}

//enh:end
}
