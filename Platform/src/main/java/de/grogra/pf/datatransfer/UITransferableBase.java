package de.grogra.pf.datatransfer;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class UITransferableBase implements UITransferable {

	Object source;
	Object content;
	String dispalyName;
	
	public UITransferableBase(Object source, Object content) {
		this.source=source;
		this.content=content;
	}
	public UITransferableBase(Object source, Object content,String displayName) {
		this.source=source;
		this.content=content;
		this.dispalyName=displayName;
	}
	
	@Override
	public DataFlavor[] getTransferDataFlavors() {
        try {
			return new DataFlavor[] { new DataFlavor(DataFlavor.javaJVMLocalObjectMimeType +
			        ";class=\"" +
			        content.getClass().getName() +
			        "\"", null, content.getClass().getClassLoader()),
					SOURCE,
					DISPLAYNAME
					};
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return new DataFlavor[0];
	}

	@Override
	public boolean isDataFlavorSupported(DataFlavor flavor) {
		for (DataFlavor f : getTransferDataFlavors()) {
			if (flavor.getRepresentationClass().isAssignableFrom(f.getRepresentationClass())) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
		if (!isDataFlavorSupported(flavor)) { return null; }
		if (flavor.equals(SOURCE)) {
			return source;
		}
		if (flavor.equals(DISPLAYNAME)) {
			return dispalyName;
		}
		
		return content;
	}

	@Override
	public Object getSource() {
		return source;
	}

	@Override
	public Object getContent() {
		return content;
	}

	@Override
	public String getDisplayName() {
		return dispalyName;
	}

}
