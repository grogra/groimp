package de.grogra.pf.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import de.grogra.pf.io.FilterSource;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.registry.RegistryContext;
import de.grogra.pf.ui.registry.ProjectFactory;
import de.grogra.util.Map;

public interface WorkbenchManager extends RegistryContext{

	
	/**
	 * return the workbench of given id
	 * 	 * @param the id of the workbench to select
	 * @return
	 */
	public ProjectWorkbench getWorkbench(Object id) throws IndexOutOfBoundsException;
	
	/**
	 * return the object workbenches 
	 */
	public ArrayList<Workbench> getWorkbenches();
	
	/**
	 * Return the list of workbench handled by the manager
	 * @return a list of Workbench (shouldn't this return Workbench[]? and an array?)
	 */
	public HashMap<Integer, Workbench> listWorkbenches();
	
	
	/**
	 * Returning the ID of a workbench
	 * @param wb
	 * @return
	 */
	
	public Object getWorkbenchId(ProjectWorkbench wb) ;
	/**
	 * Register a workbench to this manager
	 */
	void registerWorkbench(ProjectWorkbench w);
	
	/**
	 * deregister a workbench to this manager
	 */
	void deregisterWorkbench(ProjectWorkbench w);
	
	/**
	 * Dispose a workbench - should be deregistered first
	 */
	void closeWorkbench(ProjectWorkbench w);
	
	/**
	 * Dispose a workbench - should be deregistered first
	 * Execute the afterDispose command (usually a popup for saving)
	 */
	void closeWorkbench(ProjectWorkbench w, Command afterDispose);
	
	/**
	 * Check if the workbench is managed by this manager
	 * @param w
	 * @return
	 */
	boolean isManager(ProjectWorkbench w);

	/**
	 * Create a new Workbench around the Project loaded from the FilterSource
	 * The project is created in the context of the worbench manager, which is included in the context of the uiapp
	 * 
	 */
	public ProjectWorkbench createWorkbench(ProjectFactory pf, FilterSource fs, Map initParams);

	/**
	 * Create a new workbench around a new empty project
	 * 
	 * @return
	 * @throws IOException 
	 */
	public ProjectWorkbench createWorkbench(ProjectFactory pf) throws IOException;

	/**
	 * Create a new workbench around the Project of the given id
	 * 
	 * @return
	 */
	public ProjectWorkbench createWorkbench(Object projectId);

	
}
