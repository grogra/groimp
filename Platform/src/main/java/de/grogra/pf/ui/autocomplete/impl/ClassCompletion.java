package de.grogra.pf.ui.autocomplete.impl;

import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;

import de.grogra.pf.ui.autocomplete.CompletionContext;

/**
 * A completion for classes
 */
public class ClassCompletion extends AbstractCompletion {

	/**
	 * What library (for example) this variable is defined in.
	 */
	private String definedIn;
	
	/**
	 * The class that is defined.
	 */
	private Class clazz;
		
	
	private boolean isInterface;
	private boolean isEnclosed;
	private boolean isAbstract;
	
	private List<String> superclass; // full name of superclasses/interfaces
	protected List<Completion> constructors; //functionCompletion
	protected List<Completion> methods; //functioncompletion
	protected List<Completion> fields; //variablecompletion
	
	protected AbstractCompletionProvider constructorsProvider;
	protected AbstractCompletionProvider internalsProvider;
		
	private String name;
	
	private String shortDesc;
	private String summary;
	
	private CompletionContext cc;
	
	/**
	 * Constructor.
	 *
	 */
	public ClassCompletion(CompletionProvider provider, String name, CompletionContext cc) {
		super(provider);
		this.name=name;
		superclass = new ArrayList<String>();
		constructors = new ArrayList<Completion>();
		methods = new ArrayList<Completion>();
		fields = new ArrayList<Completion>();
		this.cc = cc;
	}
	
	@Override
	public String getReplacementText() {
		return name;
	}
	
	
	public void initializeProviders(CompletionProvider parent) {
		initializeProviders(parent, AbstractAutoCompletor.ICON_CLASS);
	}
	
	public void initializeProviders(CompletionProvider parent, Icon iconForConstructor) {
		constructorsProvider = new ChildrenCompletionProvider();
		constructorsProvider.setParameterizedCompletionParams('(', ", ", ')');
		constructorsProvider.setParent(parent);
		if (constructors.isEmpty()) {
			FunctionCompletion defaultconstr = new FunctionCompletion(parent, name, null);
			defaultconstr.setIcon(iconForConstructor);
			defaultconstr.setDefinedIn(definedIn);
			defaultconstr.setShortDescription("Default constructor");
			constructors.add(defaultconstr);
		}
		for (Completion c : constructors) {
			if (!isInterface || !isAbstract) {
				((AbstractCompletion)c).setIcon(iconForConstructor);
				String sd = ((FunctionCompletion)c).getShortDescription();
				if (sd==null || sd.isBlank()) {
					((FunctionCompletion)c).setShortDescription(getShortDescription());
				}
				constructorsProvider.addCompletion(c);
			}
		}
		internalsProvider = new ChildrenCompletionProvider();
		internalsProvider.setParameterizedCompletionParams('(', ", ", ')');
		internalsProvider.setParent(parent);
		internalsProvider.addCompletions(methods);
		internalsProvider.addCompletions(fields);
	}
	
	public CompletionProvider getConstructorsProvider() {
		return this.constructorsProvider;
	}
	
	public CompletionProvider getInternalsProvider() {
		return this.internalsProvider;
	}
	
	protected void addDefinitionString(StringBuilder sb) {
		sb.append("<html><b>").append(getDefinitionString()).append("</b>");
	}


	/**
	 * Return the definition of this variable completion.
	 *
	 * @return The definition string.
	 */
	public String getDefinitionString() {

		StringBuilder sb = new StringBuilder();

		// Add the name of the item being described
		sb.append(getName());

		return sb.toString();

	}

	/**
	 * in a package
	 */
	public String getDefinedIn() {
		return definedIn;
	}


	/**
	 * Returns the name of this variable.
	 *
	 * @return The name.
	 */
	public String getName() {
		return name;
	}


	@Override
	public String getSummary() {
		StringBuilder sb = new StringBuilder();
		addDefinitionString(sb);
		possiblyAddDescription(sb);
		possiblyAddDefinedIn(sb);
		return sb.toString();
	}


	/**
	 * Tooltip used when hovering on that word.
	 * Not working with current impl?
	 */
	@Override
	public String getToolTipText() {
		return getDefinitionString();
	}


	/**
	 * Adds some HTML describing where this variable is defined, if this
	 * information is known.
	 *
	 * @param sb The buffer to append to.
	 */
	protected void possiblyAddDefinedIn(StringBuilder sb) {
		if (definedIn!=null) {
			sb.append("<hr>Defined in:"); // TODO: Localize me
			sb.append(" <em>").append(definedIn).append("</em>");
		}
	}


	/**
	 * Adds the description text as HTML to a buffer, if a description is
	 * defined.
	 *
	 * @param sb The buffer to append to.
	 * @return Whether there was a description to add.
	 */
	protected boolean possiblyAddDescription(StringBuilder sb) {
		if (getShortDescription()!=null) {
			sb.append("<hr><br>");
			sb.append(getShortDescription());
			sb.append("<br><br><br>");
			return true;
		}
		return false;
	}


	/**
	 * Sets where this variable is defined.
	 *
	 * @param definedIn Where this variable is defined.
	 * @see #getDefinedIn()
	 */
	public void setDefinedIn(String definedIn) {
		this.definedIn = definedIn;
	}

	public void setClazz(Class c) {
		this.clazz = c;
	}
	
	public Class getClazz() {
		return clazz;
	}

	/**
	 * Overridden to return the name of the variable being completed.
	 *
	 * @return A string representation of this completion.
	 */
	@Override
	public String toString() {
		return getName();
	}
	
	public void addSuperClass(String sc) {
		if(sc!=null) this.superclass.add(sc);
	}
	
	public List<String> getSuperClass() {
		return superclass;
	}
	
	public void addConstructors(FunctionCompletion cc) {
		if (cc!=null) this.constructors.add(cc);
	}
	
	public void addConstructors(List<FunctionCompletion> cc) {
		if (cc!=null) this.constructors.addAll(cc);
	}
	
	public List<Completion>  getConstructors() {
		return constructors;
	}
	
	public void addMethods(FunctionCompletion mc) {
		if (mc!=null) this.methods.add(mc);
	}
	
	public void addMethods(List<FunctionCompletion> mc) {
		if (mc!=null) this.methods.addAll(mc);
	}
	
	public List<Completion> getMethods() {
		return methods;
	}
	
	public void addFields(VariableCompletion vc) {
		if (vc!=null) this.fields.add(vc);
	}
	
	public void addFields(List<VariableCompletion> vc) {
		if (vc!=null) this.fields.addAll(vc);
	}
	
	public List<Completion> getFields() {
		return fields;
	}
	
	public void setAbstract(boolean a) {
		this.isAbstract=a;
	}
	
	public boolean isAbstract() {
		return this.isAbstract;
	}

	public String getShortDescription() {
		return shortDesc;
	}

	public void setShortDescription(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
}
