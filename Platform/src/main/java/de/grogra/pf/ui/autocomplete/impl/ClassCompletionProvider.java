package de.grogra.pf.ui.autocomplete.impl;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.text.BadLocationException;

import de.grogra.pf.ui.autocomplete.AutoCompletableTextArea;
import de.grogra.pf.ui.autocomplete.CompletionContext;

/**
 * A completion provider that check the text to the carret position, then,
 * depending on the completioncontext it: 
 * if the CompletionContext.getContext() == NEWCLASS:
 * 		it try to match the ClassCompletion.getName() to the entered text
 * 		and autocomplete it with the ClassCompletion.getConstructors()
 * if the CompletionContext.getContext() == INCLASS:
 * 		it try to find match the name of the class with getEnteredClass()
 * 		if it is not null (a.k.a. a CompletionClass matches) it
 * 		will complete based on the ClassCompletion.getInternals()
 */
public class ClassCompletionProvider extends AbstractCompletionProvider {

	/**
	 * To speedup successive completions
	 */
	private String lastCompletionsAtText;
	private List<Completion> lastParameterizedCompletionsAt;
	ClassCompletion lastClassComp;
	int lastContext;
	private CompletionContext cc;

	public ClassCompletionProvider(CompletionContext cc) {
		this.cc=cc;
	}

	/**
	 * returns the text used for the completion. 
	 */
	@Override
	public String getAlreadyEnteredText(AutoCompletableTextArea comp) {
		if ( (cc.getContext() & CompletionContext.NEWCLASS) > 0) {
			return comp.getAlreadyEnteredText(this);
		}
		if ( (cc.getContext() & CompletionContext.CLASS) > 0 ) {
			return comp.getAlreadyEnteredText(this);
		}
		if ( (cc.getContext() & CompletionContext.CLASSDEF) > 0 ) {
			return comp.getAlreadyEnteredText(this);
		}
		return "";
	}

	@Override
	public boolean isValidChar(char c) {
		return Character.isAlphabetic(c) || c == '_';
	}

	@Override
	public List<Completion> getCompletionsAt(AutoCompletableTextArea comp, Point p) {
		String text = comp.getAlreadyEnteredText(this);

		if (text.equals(lastCompletionsAtText)) {
			return lastParameterizedCompletionsAt;
		}		
		List<Completion> list = getCompletionByInputText(text);
		lastCompletionsAtText = text;
		return lastParameterizedCompletionsAt = list;
	}
	
	@Override
	protected List<Completion> getCompletionsImpl(AutoCompletableTextArea comp) {
		String text = comp.getAlreadyEnteredText(this);

		
		List<Completion> results= new ArrayList<Completion>();
		ClassCompletion classComp=null;
		
		if ( (cc.getContext() & CompletionContext.CLASSDEF) > 0) {
			List<Completion> possibleClasses = super.getCompletionsImpl(comp);
			if (possibleClasses==null) {
				return results;
			}
			results = possibleClasses;
			lastContext = CompletionContext.CLASSDEF;
		}

		else if ( (cc.getContext() & CompletionContext.NEWCLASS) > 0 ) {
			if (text.equals(lastCompletionsAtText) && cc.getContext() == lastContext) {
				return lastParameterizedCompletionsAt;
			}

			List<Completion> possibleClasses = super.getCompletionsImpl(comp);
			if (possibleClasses==null) {
				return results;
			}
			for (Completion c : possibleClasses) {
				results.addAll( ((AbstractCompletionProvider)((ClassCompletion)c)
						.getConstructorsProvider()).getCompletionsImpl(comp) );
			}
			lastContext = CompletionContext.NEWCLASS;
		}
		
		else if ( (cc.getContext() & CompletionContext.CLASS) > 0) {
			classComp = getClassCompletionFromText(comp); //might work
			if (classComp==null) {
				return results;
			}
			if (classComp.equals(lastClassComp) && text.equals(lastCompletionsAtText)) {
				return lastParameterizedCompletionsAt;
			}
			results = ((AbstractCompletionProvider)classComp
					.getInternalsProvider()).getCompletionsImpl(comp);
			lastContext = CompletionContext.CLASS;
		}

		//do same with class name 
		lastCompletionsAtText = text;
		lastClassComp = classComp; // not sure how to use this
		return lastParameterizedCompletionsAt = results;
	}
	
	
	protected String getClassNameFromText(AutoCompletableTextArea comp) {
		try {
			String text = comp.getText(0, comp.getCaretPosition());
			
			char[] ctext = text.toCharArray();
			char start = 0;
			char[] filter = new char[] {' ', '\n', '_', '\t'};
			char[] filterword = new char[] {'_'};
			char[] usedfilter = filter;
			StringBuffer word = new StringBuffer();
			boolean fillWord = false;
			for (int i = ctext.length - 1; i >= 0; i--) {
				if (!Character.isLetterOrDigit(ctext[i]) && !charContains(ctext[i], usedfilter )) {
					start = ctext[i];
					if (fillWord) {
						break;
					}
					if (start == '.') { 
						fillWord=true;
						usedfilter=filterword;
					}
				}
				else {
					if (fillWord) {
						word.append(ctext[i]);
					}
				}
			}
			word.reverse();
			return word.toString();
		
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	
	protected boolean charContains(char c, char[] array) {
	    for (char x : array) {
	        if (x == c) {
	            return true;
	        }
	    }
	    return false;
	}
	


	/**
	 * return a classcompletion that matches the name of the read class from
	 * the text. Or null if no such class can be found.
	 * @return
	 */
	protected ClassCompletion getClassCompletionFromText(AutoCompletableTextArea comp) {
		String className = getClassNameFromText(comp);
		
		//for now simplify class name 
		if (className.contains(".")) {
			className = className.substring(className.lastIndexOf('.'), className.length());
		}
		
		final String cName = className;

		List<Completion> possibleClasses = completions.stream().parallel()
				.filter(c -> c instanceof ClassCompletion &&
						((ClassCompletion)c).getName().equals(cName))
				.toList();

		if (possibleClasses.isEmpty()) {
			return null;
		}
		if (possibleClasses.size()>1) {
			System.out.println("Should only be one? ");
		}

		return (ClassCompletion) possibleClasses.get(0);
	}

	@Override
	public List<ParameterizedCompletion> getParameterizedCompletions(AutoCompletableTextArea tc) {
		// TODO Auto-generated method stub
		return null;
	}

}
