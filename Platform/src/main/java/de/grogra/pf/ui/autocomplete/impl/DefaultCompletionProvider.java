package de.grogra.pf.ui.autocomplete.impl;

/*
 * 12/21/2008
 *
 * DefaultCompletionProvider.java - A basic completion provider implementation.
 *
 * This library is distributed under a modified BSD license.  See the included
 * LICENSE.md file for details.
 */

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import de.grogra.pf.ui.autocomplete.AutoCompletableTextArea;


/**
 * A basic completion provider implementation.  This provider has no
 * understanding of language semantics.  It simply checks the text entered up
 * to the caret position for a match against known completions.  This is all
 * that is needed in the majority of cases.
 *
 * @author Robert Futrell
 * @version 1.0
 */
public class DefaultCompletionProvider extends AbstractCompletionProvider {

	/**
	 * Used to speed up {@link #getCompletionsAt(AutoCompletableTextArea, Point)}.
	 */
	private String lastCompletionsAtText;

	/**
	 * Used to speed up {@link #getCompletionsAt(AutoCompletableTextArea, Point)},
	 * since this may be called multiple times in succession (this is usually
	 * called by {@code AutoCompletableTextArea.getToolTipText()}, and if the user
	 * wiggles the mouse while a tool tip is displayed, this method gets
	 * repeatedly called.  It can be costly, so we try to speed it up a tad).
	 */
	private List<Completion> lastParameterizedCompletionsAt;

	/**
	 * Constructor.  The returned provider will not be aware of any completions.
	 *
	 * @see #addCompletion(Completion)
	 */
	public DefaultCompletionProvider() {
		init();
	}


	/**
	 * Creates a completion provider that provides completion for a simple
	 * list of words.
	 *
	 * @param words The words to offer as completion suggestions.  If this is
	 *        <code>null</code>, no completions will be known.
	 * @see BasicCompletion
	 */
	public DefaultCompletionProvider(String[] words) {
		init();
		addWordCompletions(words);
	}


	/**
	 * Returns the text just before the current caret position that could be
	 * the start of something auto-completable.<p>
	 *
	 * This method returns all characters before the caret that are matched
	 * by  {@link #isValidChar(char)}.
	 * <p>
	 * {@inheritDoc}
	 */
	@Override
	public String getAlreadyEnteredText(AutoCompletableTextArea comp) {
		return comp.getAlreadyEnteredText(this);
	}

	@Override
	public List<Completion> getCompletionsAt(AutoCompletableTextArea tc, Point p) {

		String text = tc.getAlreadyEnteredText(this);

		if (text.equals(lastCompletionsAtText)) {
			return lastParameterizedCompletionsAt;
		}

		// Get a list of all Completions matching the text.
		List<Completion> list = getCompletionByInputText(text);
		lastCompletionsAtText = text;
		return lastParameterizedCompletionsAt = list;
	}


	// When is that one used? 
	@Override
	public List<ParameterizedCompletion> getParameterizedCompletions(
			AutoCompletableTextArea tc) {

		List<ParameterizedCompletion> list = null;

		// If this provider doesn't support parameterized completions,
		// bail out now.
		char paramListStart = getParameterListStart();
		if (paramListStart==0) {
			return list; // null
		}

		String text = tc.getAlreadyEnteredText(this);

		// Get a list of all Completions matching the text, but then
		// narrow it down to just the ParameterizedCompletions.
		List<Completion> l = getCompletionByInputText(text);
		if (l!=null && !l.isEmpty()) {
			for (Object o : l) {
				if (o instanceof ParameterizedCompletion) {
					if (list == null) {
						list = new ArrayList<>(1);
					}
					list.add((ParameterizedCompletion) o);
				}
			}
		}
		return list;

	}


	/**
	 * Initializes this completion provider.
	 */
	protected void init() {
	}


	/**
	 * Returns whether the specified character is valid in an auto-completion.
	 * The default implementation is equivalent to
	 * "<code>Character.isLetterOrDigit(ch) || ch=='_'</code>".  Subclasses
	 * can override this method to change what characters are matched.
	 *
	 * @param ch The character.
	 * @return Whether the character is valid.
	 */
	public boolean isValidChar(char ch) {
		return Character.isLetterOrDigit(ch) || ch=='_';
	}

}
