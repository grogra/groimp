/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.pf.ui.registry;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node.NType;
import de.grogra.persistence.Manageable;
import de.grogra.pf.io.*;
import de.grogra.pf.registry.*;
import de.grogra.pf.registry.expr.Resource;
import de.grogra.pf.ui.*;
import de.grogra.util.*;
import de.grogra.vfs.FileSystem;

public class FilterSourceFactory extends ObjectItemFactory
{
	String directory;
	//enh:field

	String mimeType;
	//enh:field

	//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field directory$FIELD;
	public static final NType.Field mimeType$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (FilterSourceFactory.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((FilterSourceFactory) o).directory = (String) value;
					return;
				case 1:
					((FilterSourceFactory) o).mimeType = (String) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((FilterSourceFactory) o).directory;
				case 1:
					return ((FilterSourceFactory) o).mimeType;
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new FilterSourceFactory ());
		$TYPE.addManagedField (directory$FIELD = new _Field ("directory", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 0));
		$TYPE.addManagedField (mimeType$FIELD = new _Field ("mimeType", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (String.class), null, 1));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new FilterSourceFactory ();
	}

//enh:end

	@Override
	protected Item createItemImpl (Context ctx)
	{
		return null;
	}


	@Override
	public Object evaluate (RegistryContext ctx, de.grogra.util.StringMap args)
	{
		
		
		URL url = null;

		for (Edge e = this.getFirstEdge(); e != null; e = e.getNext(this)){
			if (e.isSource(this)){
				if(e.getTarget() instanceof Resource) {
				url =	(URL)((Resource)e.getTarget()).evaluate(ctx, args);
				}
			}
		}
//		if (getBranch () != null)
//		{
//			Object[] a = getArgs ((Item) getBranch (), ctx, args, this);
//			if ((a.length > 0) && (a[0] instanceof URL))
//			{
//				url = (URL) a[0];
//			}
//		}
		if(url!=null) {
			return addFromURL (ctx.getRegistry (), url, args);
		}
		return null;
	}

	/**
	 * Reads an object from <code>url</code>. If necessary,
	 * an item reference to the object is created in the registry.
	 * 
	 * @param reg registry in which the reference shall be created
	 * @param url URL to read
	 * @param params parameters, may be <code>null</code>
	 * @param ui workbench to use for UI
	 * @return the import object, or <code>null</code> in case of problems
	 */
	public Object addFromURL (Registry reg, URL url, ModifiableMap params)
	{
		FilterSource fs = null;
		if (url != null)
		{
			try
			{
				MimeType mt = (mimeType != null) ? MimeType.valueOf (mimeType)
						: IO.getMimeType (url.toString ());
				File file = Utils.urlToFile (url);
				if (file.exists ())
				{
					fs = new FileSource (file, mt, reg, params);
				}
				else
				{
					fs = new InputStreamSourceImpl (url.openStream (), url
						.toString (), mt, reg, params);
				}
			}
			catch (IOException e)
			{
				return null;
			}
		}
		return fs;
	}
	

}
