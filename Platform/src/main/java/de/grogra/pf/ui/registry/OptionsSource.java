package de.grogra.pf.ui.registry;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Writer;
import java.util.TreeMap;
import java.util.Map.Entry;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;

import de.grogra.persistence.XMLPersistenceWriter;
import de.grogra.pf.io.FileSource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Option;
import de.grogra.pf.registry.OptionFileBase;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Showable;
import de.grogra.util.MimeType;
import de.grogra.util.StringMap;
import de.grogra.vfs.FileSystem;

/**
 * Text file that contains a list of properties. These properties are loaded first
 * in a workbench, instead of global properties. 
 * The OptionsSource file is added to the project filesystem. 
 * In the GUI it is modified from the preference panel. The file is a plain text so, 
 * so it can be modified outside of groimp with any text editor.
 */
public class OptionsSource extends OptionFileBase implements Showable, TreeModelListener
{	
	private transient TreeMap<String, String> optionsMap = new TreeMap<String, String>();
	private transient boolean loaded = false;

	public static final IOFlavor FLAVOR = IOFlavor.valueOf (OptionsSource.class);
	public static final MimeType MIME_TYPE
			= new MimeType ("text/options");
	
	public static class Loader extends FilterBase implements ObjectSource {

		public Loader(FilterItem item, FilterSource source) {
			super(item, source);
			setFlavor(FLAVOR);
		}
		
		public Object getObject ()
		{
			return this;
		}
		
	}
	
	private OptionsSource() {
		this(null, null);
	}
	
	public OptionsSource(String key, String systemId) {
		super(key);
		this.systemId = systemId;
	}
	
	public void loadOptions() throws IOException {
		if (loaded) {
			return;
		}
		loaded=true;
        final int lhs = 0;
        final int rhs = 1;
        
        Object file = getRegistry ().getProjectFile ((String) getSystemId());
		if (file != null)
		{
			FileSource f = new FileSource(
					getRegistry ().getFileSystem(), file, getSystemId(), MIME_TYPE, 
					getRegistry (), new StringMap());
			
	        BufferedReader  bfr = new BufferedReader(f.getReader());
	
	        String line;
	        while ((line = bfr.readLine()) != null) {
	            if (!line.startsWith("#") && !line.isEmpty()) {
	                String[] pair = line.trim().split("=");
	                if (pair.length==2) {
		                optionsMap.put(pair[lhs].trim(), pair[rhs].trim());
	                } else if (pair.length==1){
		                optionsMap.put(pair[lhs].trim(), "");
	                }
	            }
	        }
	        bfr.close();
	    }
    }
	
	public String getOptionStringValue(String key) {
		if (!loaded) {
			try {
				loadOptions();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return optionsMap.get(key);
	}
	
	public String getOptionStringValue(Option opt) {
		return getOptionStringValue(opt.getAbsoluteName());
	}
	
	@Override
	public void setOptionStringValue(Option opt, String value) {
		if (!loaded) {
			try {
				loadOptions();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		optionsMap.put(opt.getAbsoluteName(), value);
		// update the file
		Object file = getRegistry ().getProjectFile ((String) getSystemId());
		if (file!=null) {
			try {
				addOptions(getRegistry(), optionsMap);
				writeOptions(getRegistry(), file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	/**
	 * create a workbench.options file in the ctx filesystem.
	 * Return the File (either local File, or MemoryFilesystem$Entry)
	 * @param ctx
	 * @return
	 */
	public static Object create (Context ctx) {
		return create(ctx.getWorkbench ().getRegistry ());
	}
	
	/**
	 * create a workbench.options file in the ctx filesystem.
	 * Return the File (either local File, or MemoryFilesystem$Entry)
	 * @param ctx
	 * @return
	 */
	public static Object create (Registry r) {
		FileSystem fs = r.getFileSystem ();
		Item dest = r.getDirectory (Option.PROJECT_OPTIONS_PATH, null);
		Object f;
		try
		{
			f = fs.create (fs.getRoot(), Option.OPTIONS_FILE, false, false);
			initOptionsFile(r, f);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
		OptionsSource file = new OptionsSource (Option.OPTIONS_FILE, IO.toSystemId (fs, f));
		if (file != null && dest.getItem(Option.OPTIONS_FILE)==null)
		{
			dest.addUserItem (file);
		}
		return f;
	}
	
	
	/** 
	 * Add missing options from the registry r to the parentOpts map and 
	 * set the current optionMap to the result
	 * @param r
	 * @param parentOpts
	 */
	private void addOptions(Registry r, TreeMap<String, String> parentOpts) {
		Registry rr = r.getRootRegistry ();
		FileSystem fs = r.getFileSystem();
		Item[] opts = rr.findAll(ItemCriterion.INSTANCE_OF, Option.class, false);
		for (Item o : opts) {
			if (!parentOpts.containsKey(((Option)o).getAbsoluteName())) {
				parentOpts.put(((Option)o).getAbsoluteName(), 
						new XMLPersistenceWriter (null, null)
							.toString(((Option)o).getObjectType(), ((Option)o).getObject()));
			}
		}
	}
	
	/**
	 * Write the current optionsMap to the file
	 * @param file
	 * @throws IOException
	 */
	private void writeOptions (Registry r, Object file) throws IOException {
		FileSystem fs = r.getFileSystem();
		FileSource f = new FileSource(
				fs, file, IO.toSystemId(fs, file), MIME_TYPE, 
				r, new StringMap());
		try (Writer writer = f.getWriter(false)) {
			for (Entry<String, String> o : optionsMap.entrySet()) {
				writer.write(o.getKey()+"="+o.getValue()+"\n");
			}
			writer.flush();
		}
	}
	
	/**
	 * Used when creating a option file. This dump all global option into the file.
	 * @param file - the option file. On local -> java File, in memory -> MemoryFileSystem$Entry
	 * @throws IOException 
	 * 
	 */
	static private void initOptionsFile(Registry r, Object file) throws IOException {
		Registry rr = r.getRootRegistry ();
		FileSystem fs = r.getFileSystem();
		Item[] opts = rr.findAll(ItemCriterion.INSTANCE_OF, Option.class, false);
		FileSource f = new FileSource(
				fs, file, IO.toSystemId(fs, file), MIME_TYPE, 
				r, new StringMap());
		try (Writer writer = f.getWriter(false)) {
			for (Item o : opts) {
				writer.write(optionToString((Option)o));
				writer.flush();
			}
		}
	}
	
	static public String optionToString(Option o) {
		return o.getAbsoluteName() +"="+
				new XMLPersistenceWriter (null, null).toString(o.getObjectType(), o.getObject())+
				"\n";
	}
	
	@Override
	public void addRequiredFiles (java.util.Collection list)
	{
		Object f = getRegistry ().getProjectFile ((String)getSystemId ());
		if (f != null)
		{
			list.add (f);
		}
	}


	@Override
	public void show(Context ctx) {
		PanelFactory.getAndShowPanel(ctx, "/ui/panels/preferences", new StringMap());
	}
	
	@Override
	protected void activateImpl ()
	{
		Object f = getRegistry ().getProjectFile ((String)getSystemId ());
		if (f == null) {
			f = create(getRegistry ());
		}
		try {
			loadOptions();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (f!=null) {
			getRegistry ().addFileSystemListener (this);
			getRegistry ().getFileSystem ().setMimeType (f, MIME_TYPE);
		}
	}
	
	@Override
	protected void deactivateImpl ()
	{
		getRegistry ().removeFileSystemListener (this);
	}


	@Override
	public void treeNodesChanged(TreeModelEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void treeNodesInserted(TreeModelEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void treeNodesRemoved(TreeModelEvent e) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void treeStructureChanged(TreeModelEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	//	enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;


	static
	{
		$TYPE = new NType (new OptionsSource ());
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new OptionsSource ();
	}

//enh:end
}
