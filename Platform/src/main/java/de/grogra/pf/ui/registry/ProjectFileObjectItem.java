package de.grogra.pf.ui.registry;

import java.io.IOException;

import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IO;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.FilterSource.MetaDataKey;
import de.grogra.pf.ui.UI;
import de.grogra.pf.ui.Workbench;
import de.grogra.util.MimeType;
import de.grogra.util.ProgressMonitor;

/**
 * This class add an additional wrapping around FileObjectItem. FileObjectItem 
 * are files that are added to a project and most of the time loaded when the 
 * explorer are loaded (to get description). FileObjectItem also have the
 *  mimeType of the File they load. 
 * ProjectFileObjectItem has mimetype another type of object managed by groimp. 
 * The fileMimeType still represent the mimeType of the file to be loaded. 
 */
public class ProjectFileObjectItem extends FileObjectItem {

	public static MetaDataKey<MimeType> FILE_MIMETYPE = new MetaDataKey<MimeType> ("fileMimeType");
	
	MimeType fileMimeType;
	//enh:field getter

//enh:insert
//enh:begin
// NOTE: The following lines up to enh:end were generated automatically

	public static final NType $TYPE;

	public static final NType.Field fileMimeType$FIELD;

	private static final class _Field extends NType.Field
	{
		private final int id;

		_Field (String name, int modifiers, de.grogra.reflect.Type type, de.grogra.reflect.Type componentType, int id)
		{
			super (ProjectFileObjectItem.$TYPE, name, modifiers, type, componentType);
			this.id = id;
		}

		@Override
		protected void setObjectImpl (Object o, Object value)
		{
			switch (id)
			{
				case 0:
					((ProjectFileObjectItem) o).fileMimeType = (MimeType) value;
					return;
			}
			super.setObjectImpl (o, value);
		}

		@Override
		public Object getObject (Object o)
		{
			switch (id)
			{
				case 0:
					return ((ProjectFileObjectItem) o).getFileMimeType ();
			}
			return super.getObject (o);
		}
	}

	static
	{
		$TYPE = new NType (new ProjectFileObjectItem ());
		$TYPE.addManagedField (fileMimeType$FIELD = new _Field ("fileMimeType", 0 | _Field.SCO, de.grogra.reflect.ClassAdapter.wrap (MimeType.class), null, 0));
		$TYPE.validate ();
	}

	@Override
	protected NType getNTypeImpl ()
	{
		return $TYPE;
	}

	@Override
	protected de.grogra.graph.impl.Node newInstance ()
	{
		return new ProjectFileObjectItem ();
	}

	public MimeType getFileMimeType ()
	{
		return fileMimeType;
	}

//enh:end
	
	protected ProjectFileObjectItem ()
	{
		super();
	}
	
	public ProjectFileObjectItem (String systemId, MimeType fileMimeType, Object object,
			   String type, MimeType mimeType)
	{
		super (systemId, mimeType, object, type);
		this.fileMimeType = fileMimeType;
	}

	@Override
	protected Object fetchBaseObject ()
	{
		FilterSource s = IO.createPipeline
			(createFileSource (),
			 IOFlavor.valueOf (getObjectType ().getImplementationClass ()));
		if (!(s instanceof ObjectSource))
		{
			Workbench.current ().logGUIInfo
				(IO.I18N.msg ("openfile.unsupported", systemId,
							  IO.getDescription (getMimeType())));
			return null;
		}
		s.initProgressMonitor
			(UI.createProgressAdapter (Workbench.current ()));
		try
		{
			((ObjectSource) s).setMetaData(FILE_MIMETYPE, this.fileMimeType);
			return ((ObjectSource) s).getObject ();
		}
		catch (IOException e)
		{
			Workbench.current ().logGUIInfo
				(IO.I18N.msg ("openfile.failed", systemId), e);
			return null;
		}
		finally
		{
			s.setProgress (null, ProgressMonitor.DONE_PROGRESS);
		}
	}
	
	/**
	 * If the file do not exist. Raise an error when the project is loaded. (the object is
	 * not loaded at this point). This troubleshoot most fileobject error whithin the 
	 * project loading.
	 * The check is perform when the project is loaded because the actual object fetching
	 * is only done when the object is used - thus could happen after many executions.
	 */
	@Override
	protected void activateImpl ()
	{
		Object f = getRegistry ().getProjectFile ((String)getSystemId());
		if (f != null)
		{
			getRegistry ().getFileSystem ().setMimeType (f, getMimeType());
		}
		else {
			Workbench.current ().logGUIInfo
				(IO.I18N.msg ("openfile.failed", systemId));
		}
	}
	
	/**
	 * Change the way description are loaded so the object do NOT get loaded before use
	 */
	@Override
	protected Object getDerivedDescription (String type)
	{
		return null;
	}
	


}
