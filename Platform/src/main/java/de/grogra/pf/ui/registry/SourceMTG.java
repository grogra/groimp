/**
 * TODO: REMOVE THIS FILE 
 * The mtg file sources should not be in the platform plugin.
 * Moreover there is a MTGSourcefile in rgg/mtg package > what is this one?
 * 
 */
package de.grogra.pf.ui.registry;

import java.util.ArrayList;

import de.grogra.graph.impl.Edge;
import de.grogra.graph.impl.Node;
import de.grogra.graph.impl.Node.NType;
import de.grogra.persistence.ManageableType;
import de.grogra.pf.io.IO;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.TypeItem;
import de.grogra.reflect.Method;
import de.grogra.reflect.Type;
import de.grogra.util.MimeType;

public class SourceMTG extends SourceFile {

	public SourceMTG(String key, MimeType mimeType, String systemId) {
		super(key, mimeType, systemId);
	}

	
	private void reinstantiateMTG()
	{
		//begin from graph root node
		Node rootNode = getRegistry().getProjectGraph().getRoot();		
		
		//traverse graph and reinstantiate nodes
		if(rootNode!=null)
			reinstantiateMTGInternal(rootNode);
	}
	
	private boolean isObsoleteInstance(Node node)
	{		
		//if node is of a generated mtg module type
		if(isMTGNode(node))
		{
			//loop through lists of types in registry
			Item dir = getRegistry().getItem("/classes");
			for (Item c = (Item) dir.getBranch(); c != null; c = (Item) c.getSuccessor()) 
			{
				Type<?> t = (Type<?>) ((TypeItem) c).getObject();
				
				//the node's type has the same name as this type in the registry
				if(node.getNType().getName().equals(t.getName()))
				{
					//check if node is an instance of this type in the registry
					if(!t.isInstance(node))
					{
						//return true to say this node's type is obsolete.
						// i.e. modules in the generated xl file have been recompiled.
						//      this node's type is no longer recognized. to re-instantiate this node using new type.
						return true;
					}
					else
					{
						//the node's type matches what is found in the registry
						return false;
					}
				}
			}
		}
		return false;
	}
	
	private boolean isMTGNode(Node node)
	{
		NType nType = node.getNType();
		if(nType!=null)
		{
			//if(nType.getName().contains(MTGKeys.MTG_MODULE_PREFIX))
			if(nType.getName().contains("mtg_"))
				return true;
			else
				return false;
		}
		else
			return false;
	}
	
	private Type getNewType(Node node)
	{
		//loop through lists of types in registry
		Item dir = getRegistry().getItem("/classes");
		for (Item c = (Item) dir.getBranch(); c != null; c = (Item) c.getSuccessor()) 
		{
			Type<?> t = (Type<?>) ((TypeItem) c).getObject();
			
			//the node's type has the same name as this type in the registry
			if(node.getNType().getName().equals(t.getName()))
				return t;
		}
		return null;
	}
	
	public static void copyNodeFields(Node oldNode, Node newNode)
	{
		//Field[] oldf = null;
		//Field[] newf = null;
		
		//Class<?> oldInstance = oldNode.getNType().getImplementationClass();
		//Class<?> newInstance = newNode.getNType().getImplementationClass();
		//oldf = oldInstance.getDeclaredFields();
		//newf = newInstance.getDeclaredFields();
		
		NType oldType = oldNode.getNType();
		NType newType = newNode.getNType();
		
		int methodCount = oldType.getDeclaredMethodCount();
		int newMethodCount = newType.getDeclaredMethodCount();
		
		for(int m=0; m<methodCount; ++m)
		{
			//Method method = types[k].getDeclaredMethod(m);
			Method method = oldType.getDeclaredMethod(m);
			
			//set value of attribute if setter method found
			if((method.getName()).startsWith("get"))
			{
				String variableName = method.getName();
				variableName = variableName.substring(3);
				
				//find corresponding set method in new type
				for(int i=0; i<newMethodCount; ++i)
				{
					Method newMethod = newType.getDeclaredMethod(i);
					
					//if corresponding set method in new type found
					if(newMethod.getName().equals("set"+variableName))
					{
						Object[] oldParameters = new Object[0];
						Object[] newParameters = new Object[1];
						
						try {
							//set value as parameter of set method of new type
							newParameters[0] = method.invoke(oldNode, oldParameters);
							//invoke set method of new type to set value into new node
							newMethod.invoke(newNode, newParameters);
						} catch (Throwable e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} 
					}
				}
			}
		}
		try {
			
			//newNode.getUserField(9).setDouble(newNode, oldNode.getUserField(9).getDouble(oldNode));
			for (int i = 0; i < newType.getManagedFieldCount (); i++)
			{
				ManageableType.Field mf = newType.getManagedField (i);
				
				//if field is standard MTG attribute
				if(isStandardAttribute(mf.getName()))
				{
				
					ManageableType.Field mfOld = null;
					
					//find same field in old type
					for(int j=0; j<oldType.getManagedFieldCount(); ++j)
					{
						mfOld = oldType.getManagedField(j);
						if(mfOld.getName().equals(mf.getName()))
						{
							if(isStandardAttributeDouble(mfOld.getName()))
								mf.setDouble(newNode, mfOld.getDouble(oldNode));
							if(isStandardAttributeInt(mfOld.getName()))
								mf.setInt(newNode, mfOld.getInt(oldNode));
							if(isStandardAttributeObject(mfOld.getName()))
								mf.setObject(newNode, mfOld.getObject(oldNode));
						}
					}
					
					
				}
				
			}
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//MTGNode oldmtg = (MTGNode)oldNode;
		//MTGNode newmtg = (MTGNode)newNode;
		
		//MTGNode.copyStdAttributes(oldmtg, newmtg);
	}
	
	private static boolean isStandardAttribute(String featureName)
	{
		if(featureName==null)
			return false;
		
		if(
				isStandardAttributeDouble(featureName) ||
				isStandardAttributeInt(featureName) ||
				isStandardAttributeObject(featureName) 
				)
		{
			return true;
		}
		
		return false;
	}
	
	private static boolean isStandardAttributeDouble(String featureName)
	{
		if((featureName.equals("L1"))||
				(featureName.equals("L2"				))||
				(featureName.equals("L3"				))||
				(featureName.equals("DAB"				))||
				(featureName.equals("DAC"				))||
				(featureName.equals("DBC"				))||
				(featureName.equals("XX"				))||
				(featureName.equals("YY"				))||
				(featureName.equals("ZZ"				))||
				(featureName.equals("Length"))||
				(featureName.equals("Azimut"			))||
				(featureName.equals("Alpha"			))||
				(featureName.equals("AA"			))||	
				(featureName.equals("BB"				))||
				(featureName.equals("CC"				))||
				(featureName.equals("TopDia"			))||
				(featureName.equals("BotDia"		))||
				(featureName.equals("Position"		)))
				{
				return true;}
		
		return false;
	}
	
	private static boolean isStandardAttributeInt(String featureName)
	{
		if(
		(featureName.equals("Category"		))||
		(featureName.equals("Order"			)) ||
		(featureName.equals("mtgClassID"			)) ||
		(featureName.equals("mtgID"			)) ||
				(featureName.equals("mtgScale"			))||
				(featureName.equals("stdAttFlag"			))
				)
		{
			return true;
		}
		
		return false;
	}
	
	private static boolean isStandardAttributeObject(String featureName)
	{
		if(
				(featureName.equals("DirectionPrimary"	))||
				
				(featureName.equals("mtgClass"			))
				)
		{
			return true;
		}
		
		return false;
	}
	
	private void reinstantiateMTGInternal(Node node)
	{
		//Check if type for this node is obsolete
		boolean isObsolete = isObsoleteInstance(node);
		Node newNode = null;
		
		//if this node needs to be replaced
		if(isObsolete)
		{
			//buffer for all incoming/outgoing nodes and edges from this node
			ArrayList<Node> inNodes = new ArrayList<Node>();
			ArrayList<Integer> inEdges = new ArrayList<Integer>();
			
			ArrayList<Node> outNodes = new ArrayList<Node>();
			ArrayList<Integer> outEdges = new ArrayList<Integer>();
			
			//fill buffer with node references and edge types
			for(Edge e = node.getFirstEdge(); e!=null; e = e.getNext(node))
			{
				//incoming edge
				if(e.getTarget()==node)
				{
					inEdges.add(Integer.valueOf(e.getEdgeBits()));
					inNodes.add(e.getSource());
				}
				
				//outgoing edge
				if(e.getSource()==node)
				{
					outEdges.add(Integer.valueOf(e.getEdgeBits()));
					outNodes.add(e.getTarget());
				}
			}
			
			//create new node using new type
			Type newType = getNewType(node);
			try {
				newNode = (Node)(newType.newInstance());
			} catch (Throwable e) {
				e.printStackTrace();
			} 
			
			if(newNode!=null)
			{
				//copy node
				copyNodeFields(node, newNode);
				
				//connect new node to incoming buffered connections
				for(int i=0; i<inNodes.size(); ++i)
				{
					inNodes.get(i).addEdgeBitsTo(newNode, inEdges.get(i).intValue(), null);
				}
				//connect new node to outgoing buffered connections
				for(int j=0; j<outNodes.size(); ++j)
				{
					newNode.addEdgeBitsTo(outNodes.get(j), outEdges.get(j).intValue(), null);
				}
				
				//disconnect old node
				for(Edge e = node.getFirstEdge(); e!=null; e = e.getNext(node))
				{
					if(e.getSource()==node)
					{
						node.removeEdgeBitsTo(e.getTarget(), e.getEdgeBits(), null);
					}
					else if(e.getTarget()==node)
					{
						e.getSource().removeEdgeBitsTo(node, e.getEdgeBits(), null);
					}
				}
			}
		}
		else
		{
			newNode = node;
		}
		
		//Buffer for outgoing target nodes from current node
		ArrayList<Node> outNodes = new ArrayList<Node>();
		//fill buffer with node references and edge types
		for(Edge e = newNode.getFirstEdge(); e!=null; e = e.getNext(newNode))
		{
			if(e.getSource() == newNode)
			{
				outNodes.add(e.getTarget());
			}
		}
		for(int i=0; i<outNodes.size(); ++i)
		{
			Node outNode = outNodes.get(i);
			if(isMTGNode(outNode))
			{
				//do not re-visit updated MTG nodes - multiple paths to an mtg node may exist
				if(isObsoleteInstance(outNode))
				{
					reinstantiateMTGInternal(outNode);
				}
			}
			else
			{
				reinstantiateMTGInternal(outNode);
			}
		}
	}
	
	@Override
	protected void activateImpl ()
	{
		super.activateImpl();
		//yong 25 apr 2012
		//after recompilation, ensure that nodes in the graph are re-instantiated to use the new types
		if(this.getName().contains("generated.xl"))
			reinstantiateMTG();
	}
}
