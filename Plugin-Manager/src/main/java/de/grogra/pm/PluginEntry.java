package de.grogra.pm;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.grogra.pf.boot.Main;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.PluginDescriptor;
import de.grogra.pf.registry.Registry;
import de.grogra.pf.ui.Workbench;
import de.grogra.pm.Version.VersionListener;
import de.grogra.pm.cache.PluginsCache;
import de.grogra.pm.exception.DownloadException;
import de.grogra.pm.exception.PluginException;
import de.grogra.pm.repo.DirectorySource;
import de.grogra.pm.repo.RepoManager;
import de.grogra.pm.repo.RepoSource;
import de.grogra.pm.util.CacheUtils;
import de.grogra.pm.util.PluginEntryUtils;
import de.grogra.pm.util.RepoSourceUtils;
import de.grogra.util.Described;
import de.grogra.util.Utils;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class PluginEntry {
	private static final Pattern dependsParser = Pattern.compile("([^=<>]+)([=<>]+[0-9.]+)?");
	public static final String VER_STOCK = "0.0.0-STOCK";
	protected JSONObject versions = new JSONObject();
	protected String id;
	protected String name;
	protected String description;
	protected String helpUrl;
	protected String vendor;
	protected static String[] dependencies;
	protected String libsRepo;
	protected static String[] excluded;

	protected String mainClass;
	protected String installedPath;
	protected String installedVersion;
	protected String tempName;
	protected String destName;
	protected Version candidateVersion = new Version("");
	protected String enforcedVersion;
	protected boolean canUninstall = true;
	private String searchIndexString;
	protected boolean isDisabled=false;

	public PluginEntry(String aId) {
		id = aId;
	}

	public static PluginEntry fromJSON(JSONObject elm) {
		PluginEntry inst = new PluginEntry(elm.getString("id"));
		if (!(elm.isNull("mainClass"))) {
			inst.mainClass = elm.getString("mainClass");
		}

		if (elm.get("versions") instanceof JSONObject) {
			inst.versions = elm.getJSONObject("versions");
		}
		inst.name = elm.getString("name");
		inst.description = elm.getString("description");
		inst.helpUrl = elm.getString("helpUrl");
		inst.vendor = elm.getString("vendor");
		if (elm.has("canUninstall")) {
			inst.canUninstall = elm.getBoolean("canUninstall");
		}
		return inst;
	}
	
	public static String getDefaultDescriptionKey(PluginDescriptor pd) {
		return "/plugins/"+pd.getName()+"."+de.grogra.util.Described.SHORT_DESCRIPTION;
	}
	
	public static String getDefaultDeletionPermissionKey(PluginDescriptor pd) {
		return "/plugins/"+pd.getName()+"."+"DeletePermission";
	}

	public static PluginEntry fromPluginDescriptor(PluginDescriptor pd) {
		PluginEntry inst = new PluginEntry((String) pd.get("pluginId", RepoSourceUtils.getDefaultPluginIdFromName(pd)));

		inst.versions = RepoSourceUtils.versionsJSONfromPluginDescriptor(pd);
		inst.installedVersion = (pd.getPluginState() != PluginDescriptor.ERROR) ? pd.getPluginVersion() : null;
		inst.candidateVersion = new Version(pd.getPluginVersion());

		inst.installedPath = (pd.getPluginState() != PluginDescriptor.ERROR)
				? pd.getFileSystem().getRoot().toString()
				: null;
		inst.name = pd.getPluginName();
		inst.isDisabled = (pd.getPluginState() == PluginDescriptor.DISABLED);
		
		try {
			if (inst.isDisabled) {
				inst.description = pd.getI18NBundle().getString(getDefaultDescriptionKey(pd), "");
			}
			else {
				inst.description = (String) pd.getDescription(de.grogra.util.Described.SHORT_DESCRIPTION);
			}
		} catch (NullPointerException e) {
			inst.description = "";
		}

		inst.helpUrl = "https://gitlab.com/grogra/groimp/";
		inst.vendor = pd.getPluginProvider();
		String uninstallString = pd.getI18NBundle().getString(getDefaultDeletionPermissionKey(pd), "true");
		inst.canUninstall = Boolean.valueOf(uninstallString);
		return inst;
	}

	@Override
	public String toString() {
		return id;
	}

	public void detectInstalled() {
		detectInstalledPlugin();

		if (isInstalled()) {
			candidateVersion.setValue(installedVersion);
		} else {
			candidateVersion.setValue(getMaxVersion());
		}
	}

	private void detectInstalledPlugin() {
		Registry r = Main.getRegistry();

		PluginDescriptor pd = (PluginDescriptor) Item.findFirst(r, "/plugins", new ItemCriterion() {
			@Override
			public boolean isFulfilled(Item item, Object info) {
				return (item instanceof PluginDescriptor)
						? ((PluginDescriptor) item).getDescription(Described.NAME).equals(info)
						: false;
			}

			@Override
			public String getRootDirectory() {
				return null;
			}
		}, id, false);

		if (pd != null) {
			installedPath = pd.getFileSystem().getRoot().toString();
			installedVersion = pd.getPluginVersion();
		}
	}

	public boolean isVersionFrozenToGroIMP() {
		return versions.has("");
	}

	public boolean hasEnforcedVersion() {
		return !enforcedVersion.isEmpty();
	}

	public String getEnforcedVersion() {
		return enforcedVersion;
	}

	public void setEnforcedVersion(String version) {
		this.enforcedVersion = version;
	}

	/**
	 * Set the value of the candidate version to the highest of the "selected"
	 * listeners (jcheckbox). So the highest value of the selected box (between
	 * installed, update, and new)
	 */
	public void rollbackCandidateVersion(boolean reload) {
		if (!reload) {
			return;
		}
		String oldValue = VER_STOCK;
		VersionComparator c = new VersionComparator();
		for (VersionListener l : this.candidateVersion.getListeners()) {
			oldValue = (c.compare(oldValue, l.getVersion()) < 0) ? l.getVersion() : oldValue;
		}
		if (!oldValue.contentEquals(VER_STOCK) && !oldValue.isEmpty())
			this.candidateVersion.setValue(oldValue);
	}

	/**
	 * The dependency resolving is messing with the candidate & enforced version: It
	 * changes the versions depending on what is possible based on the dependecies
	 * of the plugin to be changed. This reset is performed at every change from the
	 * plugin manager (usually from the dialog). It ensure that the candidate
	 * version of the plugin entry matches the one from the dialog
	 * 
	 * @param skip Should the reset reload value from the listeners ?
	 */
	public void resetVersionsCandidate(boolean reload) {
		setEnforcedVersion("");
		rollbackCandidateVersion(reload);
	}

	public String getMaxVersion() {
		Set<String> versions = getVersions();
		if (versions.size() > 0) {
			String[] vers = versions.toArray(new String[0]);
			return vers[vers.length - 1];
		}
		return null;
	}

	public Set<String> getVersions() {
		Set<String> versions = new TreeSet<>(new VersionComparator());
		for (Object o : this.versions.keySet()) {
			if (o instanceof String) {
				String ver = (String) o;
				if (ver.isEmpty()) {
					versions.add(getGroIMPVersion());
				} else {
					versions.add(ver);
				}
			}
		}

		if (isInstalled()) {
			versions.add(installedVersion);
		}
		return versions;
	}

	public static String getGroIMPVersion() {
		String ver = de.grogra.pf.boot.Main.getVersion();
		String[] parts = ver.split(" ");
		if (parts.length > 1) {
			return parts[0];
		}

		return ver;
	}

//    public static String getVersionFromPath(String installedPath) {
//        Pattern p = Pattern.compile("-([\\.0-9a-zA-Z]+(-[\\w]+)?)");
//        Matcher m = p.matcher(installedPath);
//        if (m.find()) {
//            return m.group(1);
//        }
//        return VER_STOCK;
//    }

	public static String getLibInstallPath(String lib) {
		String[] cp = dependencies;
		String path = getLibPath(lib, cp);
		if (path != null)
			return path;
		return null;
	}

	public static String getLibPath(String lib, String[] paths) {
		for (String path : paths) {
			Pattern p = Pattern.compile("\\W" + lib + "-([0-9]+\\..+).jar");
			Matcher m = p.matcher(path);
			if (m.find()) {
				Main.getLogger().info("Found library " + lib + " at " + path);
				return path;
			}
		}
		return null;
	}

	public String getID() {
		return id;
	}

	public String getInstalledPath() {
		return installedPath;
	}

	public String getDestName() {
		return destName;
	}

	public String getTempName() {
		return tempName;
	}

	public String getLibRepo() {
		return libsRepo;
	}

	public boolean isInstalled() {
		return installedPath != null;
	}
	
	public boolean isDisabled() {
		return isDisabled;
	}
	
	/**
	 * The state of the plugin follows the plugin manager action for plugins.
	 * INSTALL if the plugin is installed, DISABLE is the plugin is disabled, and UNSINSTALL if 
	 * the plugin is not installed
	 * @return
	 */
	public int getState() {
		if (isInstalled() && !isDisabled()) {
			return PluginManager.INSTALL;
		}
		else if (isInstalled() && isDisabled()) {
			return PluginManager.DISABLE;
		}
		else {
			return PluginManager.UNINSTALL;
		}
	}

	public void download(RepoManager manager, GenericCallback<String> notify) throws IOException, URISyntaxException {

		String location = null;
		String version = getCandidateVersion();
		URI uri = new URI(name + "-" + version);

		PluginsCache repo = null;
		if (CacheUtils.isUsingCache()) {
			repo = (PluginsCache) CacheUtils.getCache(uri, CacheUtils.PLUGIN);
			if (repo != null && repo.isActual()) {
				location = repo.getCachedObject();
				libsRepo = null; // the libs should be in the pluginrepo already
			}
		}
		if (repo == null || location == null) {
			// if (getLibs not empty -> create a temp repo
			// add libs to libsRepo
			// tempName .getparent / Download libs to that folder
			downloadLibs(manager, notify);
			location = getDownloadUrl(version);
		}

		RepoSource.DownloadResult dwn = manager.getJar(id, location, notify);

		tempName = dwn.getTmpFile();
		if (!checkZip(version)) {
			throw new DownloadException(PluginManager.I18N.msg("error.download.missmatch", dwn.getName()));
		}
		if (CacheUtils.isUsingCache()) {
			cachePlugin(uri, dwn);
		}
		destName = getDestinationPath(dwn);
	}

	private String getDestinationPath(RepoSource.DownloadResult dwn) throws IOException {
		String user_plugin_path;
		String default_plugin_path = new File(de.grogra.pf.boot.Main.getConfigurationDirectory(), "plugins").toString();
		try {
			Item opts = Item.resolveItem(Workbench.current(), "/pluginmanager/options");
			user_plugin_path = (String) Utils.get(opts, "userpluginpath", default_plugin_path);
		} catch (NullPointerException e) {
			user_plugin_path = default_plugin_path;
		} 
		if (user_plugin_path.isEmpty()){
			user_plugin_path = default_plugin_path;
		}
		user_plugin_path =user_plugin_path.trim();
		File user_plugin_dir = new File(user_plugin_path);
		if (!user_plugin_dir.isDirectory()) {
			user_plugin_dir.delete();
		}
		if (!user_plugin_dir.exists()) {
			user_plugin_dir.mkdirs();
		}

		return URLDecoder.decode(user_plugin_dir.toString(), "UTF-8") + File.separator + dwn.getName();
	}

	private void downloadLibs(RepoManager manager, GenericCallback<String> notify) throws IOException {
		String version = getCandidateVersion();
		Map<String, String> libs = getLibs(version);
		if (libs.isEmpty()) {
			return;
		}
		Path dir = Files.createTempDirectory("gipm-" + id);
		for (Entry<String, String> e : libs.entrySet()) {
			RepoSource.DownloadResult dwn = manager.getJar(e.getKey(), e.getValue(), notify);
			Files.move(Paths.get(dwn.getTmpFile()), dir.resolve(e.getKey()), StandardCopyOption.REPLACE_EXISTING);
		}
		libsRepo = dir.toString();
	}

	private void cacheLibs(File dir) throws IOException {
		FileUtils.copyDirectory(new File(libsRepo), dir.getParentFile());
	}

	/**
     * 
     * @param uri uri to the final file (cachepath/cacheplugin/PLUGINNAME-VER/hex())
     * @param dwn the downloaded plugin
     * @throws IOException 
     */
    private void cachePlugin(URI uri, RepoSource.DownloadResult dwn) throws IOException {
    	PluginsCache old = (PluginsCache) CacheUtils.getCache(uri, CacheUtils.PLUGIN);
        if (old != null && old.isActual()) {
        	// Cache is up to date - do nothing 
        	return;
        } else {
        	long date = System.currentTimeMillis();
            long lastModified = System.currentTimeMillis();
            long expirationTime = 0;
            PluginsCache repo = new PluginsCache(
            		CacheUtils.generateCacheFile(uri, CacheUtils.PLUGIN).toString(), 
            		expirationTime, lastModified);
            repo.saveToFile(CacheUtils.generateCacheFile(uri, CacheUtils.PLUGIN), dwn);
            if (libsRepo!=null) {
            	cacheLibs(CacheUtils.generateCacheFile(uri, CacheUtils.PLUGIN));
            }
            
        }
    }

	/**
	 * Cache the installed version of the plugin to the cache folder Should not
	 * require to manage the libs as they should be included in the repo already
	 * 
	 * @throws IOException
	 * @throws URISyntaxException
	 */
	public void cachePlugin() throws IOException, URISyntaxException {
		URI uri = new URI(getName() + "-" + getInstalledVersion());
		RepoSource tmp = new DirectorySource(null);
		RepoSource.DownloadResult dwn = tmp.new DownloadResult(getInstalledPath(), uri.toString());
		PluginsCache repo = new PluginsCache(
				CacheUtils.generateCacheFile(uri, CacheUtils.PLUGIN).toString(), 
				0, System.currentTimeMillis());
		repo.saveToFile(CacheUtils.generateCacheFile(uri, CacheUtils.PLUGIN), dwn);
	}

	/**
	 * @param version
	 * @return
	 */
	public String getDownloadUrl(String version) {
		String location;
		if (isVersionFrozenToGroIMP()) {
			String downloadUrl = versions.getJSONObject("").getString("downloadUrl");
			location = String.format(downloadUrl, getGroIMPVersion());
		} else {
			if (!versions.has(version)) {
				throw new IllegalArgumentException("Version " + version + " not found for plugin " + this);
			}
			location = versions.getJSONObject(version).getString("downloadUrl");
		}
		return location;
	}

	/**
	 * Test the downloaded zip (jar) by loading the plugin in a temporary Groimp
	 * Plugin. The version of hte plugin is tested. This test required a groimp
	 * environment (aka a Workbench.current to be executed). The safeDeleter run
	 * outside of a groimp environment
	 * 
	 * @param version
	 * @return
	 * @throws IOException
	 */
	// TODO: md5 + check plugin names and install
	// for now only check that plugin loads & its version - but should check some
	// md5
	public boolean checkZip(String version) throws IOException {
		File src = new File(tempName);
		if (src.exists() && PluginEntryUtils.isZip(src)) {
			try (ZipFile zipFile = new ZipFile(tempName)) {
				Enumeration<? extends ZipEntry> entries = zipFile.entries();

				while (entries.hasMoreElements()) {
					ZipEntry entry = entries.nextElement();
					if (entry.getName().equals("plugin.xml")) {
						InputStream in = zipFile.getInputStream(entry);
						PluginDescriptor pd = PluginDescriptor.read("temp-" + name, new BufferedInputStream(in), null,
								null);

						in.close();
						if (pd.getPluginVersion().equals(version)) {
							return true;
						}
					}
				}
			}
		} else if (src.exists() && !src.isDirectory()) {
//    		File pluginxml = new File(src.getParentFile(), "plugin.xml");
			Collection<File> pluginFiles = FileUtils.listFiles(src.getParentFile(), new TrueFileFilter() {
				public boolean accept(File file) {
					return file.getName().equals("plugin.xml");
				}
			}, TrueFileFilter.INSTANCE);
			File pluginxml = pluginFiles.iterator().next();
			InputStream in = new FileInputStream(pluginxml);
			PluginDescriptor pd = PluginDescriptor.read("temp-" + name, new BufferedInputStream(in), null, null);
			in.close();
			if (pd.getPluginVersion().equals(version)) {
				return true;
			}
		}
		return false;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getHelpLink() {
		return helpUrl;
	}

	public String getVendor() {
		return vendor;
	}

	public String getCandidateVersion() {
		return candidateVersion.getValue();
	}

	public void addCandidateVersionListener(VersionListener listener) {
		candidateVersion.addListener(listener);
	}

	public void removeCandidateVersionListener(VersionListener listener) {
		candidateVersion.removeListener(listener);
	}

	public boolean canUninstall() {
		try {
			Item opts = Item.resolveItem(Main.getRegistry(), "/pluginmanager/options");
			return (Boolean) Utils.get(opts, "canDeteteRequired", false) | canUninstall;
		} catch (NullPointerException e) {
			return canUninstall;
		} 
	}

	public String getInstalledVersion() {
		return installedVersion;
	}

	public String getMainClass() {
		return mainClass;
	}

	public void setCandidateVersion(String candidateVersion) {
		this.candidateVersion.setValue(candidateVersion);
	}

	/**
	 * Set the candidate version to a new value. If the condition require the
	 * version to be equal - then also set the enforced version
	 */
	public void setCandidateVersion(String condition, String candidateVersion) {
		this.candidateVersion.setValue(candidateVersion);
		if (isConditionEqual(condition))
			this.enforcedVersion = candidateVersion;
	}

	public boolean isUpgradable() {
		if (!isInstalled()) {
			return false;
		}

		VersionComparator comparator = new VersionComparator();
		return comparator.compare(getInstalledVersion(), getMaxVersion()) < 0;
	}

	public boolean isInstalledVersionHigherThan(String version) {
		if (!isInstalled()) {
			return false;
		}
		VersionComparator comparator = new VersionComparator();
		return comparator.compare(version, getInstalledVersion()) <= 0;
	}

	public boolean isCandidateVersionHigherThan(String version) {
		if (!isInstalled()) {
			return false;
		}
		VersionComparator comparator = new VersionComparator();
		return comparator.compare(version, getCandidateVersion()) <= 0;
	}

	/**
	 * Return true if the version (installed?) fulfill the condition
	 * 
	 * @param condition a string that is composed of the 3 char =, <, >
	 * @param version   a string of a version to test against the plugin version
	 * @param installed True if the version compared is the installed one. False if
	 *                  the version compared is the candidate one.
	 * @return
	 */
	public boolean isConditionRespected(String condition, String version, boolean installed) {
		if (installed) {
			if (isConditionEqual(condition)) {
				return version.contentEquals(getInstalledVersion());
			}
			if (isConditionHigher(condition)) {
				return isInstalledVersionHigherThan(version);
			}
		} else {
			if (isConditionEqual(condition)) {
				return version.contentEquals(getCandidateVersion());
			}
			if (isConditionHigher(condition)) {
				return isCandidateVersionHigherThan(version);
			}
		}
		return false;
	}

	private boolean isConditionEqual(String condition) {
		return condition.contentEquals("=");
	}

	// no condition is higher that condition
	private boolean isConditionHigher(String condition) {
		return (condition.isEmpty()) ? true : condition.contains("<");
	}

	/**
	 * Return true if the plugin can provide a version that fulfill the condition.
	 * If the plugin has an enforced version (aka. version that Need to be used from
	 * dependency) it checks if that enforced version is the tested one. If the
	 * condition look for a higher than version, test if the maxversion could
	 * fulfill the version;
	 * 
	 * @param condition
	 * @param version
	 * @return
	 */
	public boolean hasVersion(String condition, String version) {
		if (hasEnforcedVersion()) {
			return getEnforcedVersion().equals(version);
		}
		if (isConditionEqual(condition)) {
			return getVersions().contains(version);
		}
		if (isConditionHigher(condition)) {
			VersionComparator comparator = new VersionComparator();
			return comparator.compare(version, getMaxVersion()) <= 0;
		}
		return false;
	}

	public Map<String, String> getDepends() throws JSONException {
		Map<String, String> depends = new HashMap<>();
		if (!versions.has(getCandidateVersion())) {
			throw new PluginException(getName() + " cannot be loaded at version: "+ getCandidateVersion());
		}
		JSONObject version = versions.getJSONObject(getCandidateVersion());
		if (version.has("depends")) {
			JSONObject list = version.getJSONObject("depends");
			for (Object o : list.keySet()) {
				if (o instanceof String) {
					String dep = (String) o;
					Matcher m = dependsParser.matcher(dep);
					if (!m.find()) {
						throw new IllegalArgumentException("Cannot parse depend str: " + dep);
					}
					depends.put(dep, list.getString(dep));
				}
			}
		}
		return depends;
	}

	public Set<String> getExcluded() {
		Set<String> excluded = new HashSet<>();
		JSONObject version = versions.getJSONObject(getCandidateVersion());
		if (version.has("excludes")) {
			JSONArray list = version.getJSONArray("excludes");
			for (Object o : list) {
				if (o instanceof String) {
					String excl = (String) o;
					Matcher m = dependsParser.matcher(excl);
					if (!m.find()) {
						throw new IllegalArgumentException("Cannot parse exclude str: " + excl);
					}
					excluded.add(m.group(1));
				}
			}
		}
		return excluded;
	}

	public Map<String, String> getLibs(String verStr) {
		Map<String, String> depends = new HashMap<>();
		JSONObject version = versions.getJSONObject(isVersionFrozenToGroIMP() ? "" : verStr);
		if (version.has("libs")) {
			JSONObject list = version.getJSONObject("libs");
			for (Object o : list.keySet()) {
				if (o instanceof String) {
					String dep = (String) o;
					depends.put(dep, list.getString(dep));
				}
			}
		}
		return depends;
	}

	public Map<String, String> getRequiredLibs(String verStr) {
		Map<String, String> libs = getLibs(verStr);
		Map<String, String> requiredLibs = new HashMap<>();
		for (String libName : libs.keySet()) {
			if (libName.contains(">=")) {
				requiredLibs.put(libName, libs.get(libName));
			}
		}
		return requiredLibs;
	}

	public String getVersionChanges(String versionStr) {
		JSONObject version = versions.getJSONObject(versionStr);
		return version.has("changes") ? version.getString("changes") : null;
	}

	private class VersionComparator implements java.util.Comparator<String> {
		@Override
		public int compare(String a, String b) {
			String[] aParts = a.split("\\W+");
			String[] bParts = b.split("\\W+");

			for (int aN = 0; aN < aParts.length; aN++) {
				if (aN < bParts.length) {
					int res = compare2(aParts[aN], bParts[aN]);
					if (res != 0)
						return res;
				}
			}

			return a.compareTo(b);
		}

		private int compare2(String a, String b) {
			if (a.equals(b)) {
				return 0;
			}

			Object ai, bi;
			try {
				ai = Integer.parseInt(a);
			} catch (NumberFormatException e) {
				ai = a;
			}

			try {
				bi = Integer.parseInt(b);
			} catch (NumberFormatException e) {
				bi = b;
			}

			if (ai instanceof Integer && bi instanceof Integer) {
				return Integer.compare((Integer) ai, (Integer) bi);
			} else if (ai instanceof String && bi instanceof String) {
				return ((String) ai).compareTo((String) bi);
			} else if (ai instanceof String) {
				return 1;
			} else {
				return -1;
			}
		}
	}

	public String getSearchIndexString() {
		if (searchIndexString == null || searchIndexString.isEmpty()) {
			StringBuilder builder = new StringBuilder();
			builder.append(id);
			builder.append(name);
			builder.append(description);
			searchIndexString = builder.toString().toLowerCase();
		}

		return searchIndexString;
	}

}
