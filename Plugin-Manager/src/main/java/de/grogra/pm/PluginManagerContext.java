package de.grogra.pm;

/**
 * Context of elements that are managed with a plugin manager. 
 */
public interface PluginManagerContext {
	
	public PluginManager getPluginManager();
}
