package de.grogra.pm.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.apache.commons.lang3.StringUtils;

import de.grogra.pf.boot.Main;
import de.grogra.pm.GenericCallback;
import de.grogra.pm.PluginEntry;
import de.grogra.pm.gui.PluginCheckbox.StateListener;
import de.grogra.pm.util.DisplayUtils;
import de.grogra.pm.util.PlaceholderTextField;

/*
 * The PluginManager and most classes in the plug in are comes from JMeter PluginManager.
 * (https://github.com/undera/jmeter-plugins-manager)
 * The code have been adapted for GroIMP.
 */
public class PluginsList extends JPanel implements ListSelectionListener, HyperlinkListener {
    private static final long serialVersionUID = 295116233618658217L;

    private final JTextPane description = new JTextPane();
    protected final PlaceholderTextField searchField = new PlaceholderTextField();
    private final DefaultListModel<PluginCheckbox> searchResults = new DefaultListModel<>();
    protected JList<PluginCheckbox> list = new CheckBoxList<>(5);
    private DefaultListModel<PluginCheckbox> listModel = new DefaultListModel<>();
    protected final JComboBox<String> version = new JComboBox<>();
    private ItemListener itemListener = new VerChoiceChanged();
    private GenericCallback<Object> dialogRefresh;
    private boolean canHalfstate;
    
    public PluginsList(GenericCallback<Object> dialogRefresh, boolean canHalfstate) {
        super(new BorderLayout(5, 0));
        this.dialogRefresh = dialogRefresh;
        this.canHalfstate = canHalfstate;

        description.setContentType("text/html");
        description.setEditable(false);
        description.addHyperlinkListener(this);

        list.setModel(listModel);
        list.setBorder(PluginManagerDialog.SPACING);
        list.addListSelectionListener(this);

        add(getPluginsListComponent(), BorderLayout.WEST);
        add(getDetailsPanel(), BorderLayout.CENTER);

        list.setComponentPopupMenu(new ToggleAllPopupMenu());
        
    }

    private Component getPluginsListComponent() {
        initSearchField();
        JPanel topAndDown = new JPanel(new BorderLayout(5, 0));
        topAndDown.add(searchField, BorderLayout.NORTH);
        topAndDown.add(new JScrollPane(list));
        return topAndDown;
    }

    private void initSearchField() {
        searchField.setPlaceholder("Search...");
        searchField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                // NOOP.
            }

            @Override
            public void keyPressed(KeyEvent e) {
                // NOOP.
            }

            @Override
            public void keyReleased(KeyEvent e) {
                filterPluginsList();
            }
        });
    }

    private void filterPluginsList() {
        final String filter = searchField.getText().toLowerCase();
        if (!filter.isEmpty()) {
            searchResults.clear();
            for (int i = 0; i < listModel.size(); i++) {
                PluginCheckbox pluginCheckbox = listModel.getElementAt(i);
                PluginEntry plugin = pluginCheckbox.getPlugin();
                final String data = plugin.getSearchIndexString();
                if (data.contains(filter)) {
                    searchResults.addElement(pluginCheckbox);
                }
            }
            list.setModel(searchResults);
        } else {
            list.setModel(listModel);
        }
    }

    public void setPlugins(Set<PluginEntry> plugins, StateListener checkboxNotifier) {
    	disposeOld();
    	listModel.clear();
    	List<PluginCheckbox> p = new ArrayList<PluginCheckbox>();
        for (PluginEntry plugin : plugins) {
        	p.add(getCheckboxItem(plugin, checkboxNotifier));
        }
        Collections.sort(p);
        listModel.addAll(p);
    }

    private JPanel getDetailsPanel() {
        JPanel detailsPanel = new JPanel(new BorderLayout());
        detailsPanel.add(new JScrollPane(description), BorderLayout.CENTER);

        version.setEnabled(false);

        JPanel verPanel = new JPanel(new BorderLayout());
        verPanel.add(new JLabel("Version: "), BorderLayout.WEST);
        verPanel.add(version, BorderLayout.CENTER);
        detailsPanel.add(verPanel, BorderLayout.SOUTH);
        return detailsPanel;
    }

    protected PluginCheckbox getCheckboxItem(PluginEntry plugin, StateListener changeNotifier) {
        PluginCheckbox element = new PluginCheckbox(plugin.getName(), canHalfstate);
        element.setState( (!plugin.isInstalled())? PluginCheckbox.UNSELECTED : 
        	(plugin.isDisabled()) ? PluginCheckbox.HALFSELECTED : PluginCheckbox.SELECTED);
        element.setPlugin(plugin);
        element.addStateListener(changeNotifier);
        element.setSelectedVersion( (!plugin.isInstalled())? PluginEntry.VER_STOCK: 
        	(plugin.isDisabled()) ? PluginEntry.VER_STOCK : plugin.getInstalledVersion());
        return element;
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (!e.getValueIsAdjusting() && list.getSelectedIndex() >= 0) {
        	PluginEntry plugin = list.getSelectedValue().getPlugin();
            description.setText(DisplayUtils.getDescriptionHTML(plugin));
            setUpVersionsList(list.getSelectedValue());
            setToolTipRenderer(plugin);
            description.setCaretPosition(0);
        }
    }

    private void setToolTipRenderer(PluginEntry plugin) {
        final List<String> tooltips = new ArrayList<>();

        for (String version : plugin.getVersions()) {
            tooltips.add(plugin.getVersionChanges(version));
        }

        version.setRenderer(new ComboboxToolTipRenderer(tooltips));
    }

    protected void setUpVersionsList(PluginCheckbox cb) {
        version.removeItemListener(itemListener);
        version.removeAllItems();
        for (String ver : cb.getPlugin().getVersions()) {
            version.addItem(ver);
        }
        version.setSelectedItem(getCbVersion(cb));

        version.setEnabled(version.getItemCount() > 1);
        version.addItemListener(itemListener);
    }

    protected String getCbVersion(PluginCheckbox cb) {
    	PluginEntry plugin = cb.getPlugin();
        if (plugin.isInstalled()) {
            return plugin.getInstalledVersion();
        } else {
            return plugin.getCandidateVersion();
        }
    }

    private String getMavenInfo(PluginEntry plugin) {
        String txt = "";
        if (plugin.getCandidateVersion() != null) {
            String downloadUrl = plugin.getDownloadUrl(plugin.getCandidateVersion());
            int indexOfFP = downloadUrl.indexOf("filepath=");
            if (indexOfFP > 0) {
                String artifactUrl = downloadUrl.substring(indexOfFP + "filepath=".length());
                String[] parts = artifactUrl.split("/");
                String lastVersionId = parts[parts.length - 2];
                String artifactId = parts[parts.length - 3];
                StringBuilder groupId = new StringBuilder();
                for (int i = 0; i < parts.length - 3; i++) {
                    groupId.append(parts[i]).append(".");
                }

                if (!StringUtils.isEmpty(groupId)) {
                    txt += "<p>Maven groupId: <i>" + groupId.substring(0, groupId.length() - 1) + "</i>" +
                            ", artifactId: <i>" + artifactId + "</i>" +
                            ", version: <i>" + lastVersionId + "</i>" +
                            "</p>";
                }
            }
        }
        return txt;
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            openInBrowser(e.getURL().toString());
        }
    }

    public static void openInBrowser(String string) {
        if (java.awt.Desktop.isDesktopSupported()) {
            try {
                java.awt.Desktop.getDesktop().browse(new URI(string));
            } catch (IOException | URISyntaxException ignored) {
                Main.getLogger().warning("Failed to open in browser" + ignored);
            }
        }
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        list.setEnabled(enabled);
        version.setEnabled(enabled);
        for (PluginCheckbox ch : Collections.list(listModel.elements())) {
            ch.setEnabled(enabled);
        }
    }

    private class VerChoiceChanged implements ItemListener {
        @Override
        public void itemStateChanged(ItemEvent event) {
            if (event.getStateChange() == ItemEvent.SELECTED) {
                if (event.getItem() instanceof String) {
                    String item = (String) event.getItem();
                    PluginEntry plugin = list.getSelectedValue().getPlugin();
                    plugin.setCandidateVersion(item);
                    list.getSelectedValue().setSelectedVersion(item);
                    dialogRefresh.notify(this);
                    description.setText(DisplayUtils.getDescriptionHTML(plugin));
                    description.setCaretPosition(0);
                }
            }
        }
    }

    private class ToggleAllPopupMenu extends JPopupMenu implements ActionListener {
        /**
         *
         */
        private static final long serialVersionUID = -4299203920659842279L;

        public ToggleAllPopupMenu() {
            super("Toggle All");
            JMenuItem menuItem = new JMenuItem("Toggle All");
            menuItem.addActionListener(this);
            add(menuItem);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            for (Object a : listModel.toArray()) {
                if (a instanceof PluginCheckbox) {
                    PluginCheckbox cb = (PluginCheckbox) a;
                    cb.doClick();
                }
            }
            list.repaint();
        }
    }

    private class ComboboxToolTipRenderer extends DefaultListCellRenderer {
        private final List<String> tooltips;

        public ComboboxToolTipRenderer(List<String> tooltips) {
            this.tooltips = tooltips;
        }

        @Override
        public Component getListCellRendererComponent(JList list, Object value,
                                                      int index, boolean isSelected, boolean cellHasFocus) {

            JComponent comp = (JComponent) super.getListCellRendererComponent(list,
                    value, index, isSelected, cellHasFocus);

            if (-1 < index && null != value && null != tooltips && tooltips.size() > index) {
                list.setToolTipText(tooltips.get(index));
            }
            return comp;
        }
    }
    
    public void disposeOld() {
    	for (int i=0; i<listModel.getSize(); i++) {
    		listModel.getElementAt(i).dispose();
    	}
    }
}

