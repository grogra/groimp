// $ANTLR 2.7.7 (2006-11-01): "Compiler.tree.g" -> "Compiler.java"$


/*
 * Copyright (C) 2002 - 2007 Lehrstuhl Grafische Systeme, BTU Cottbus
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

package de.grogra.xl.compiler;

import java.util.List;
import java.lang.reflect.Array;

import de.grogra.util.*;
import de.grogra.xl.expr.*;
import de.grogra.xl.query.*;
import de.grogra.xl.util.*;
import de.grogra.xl.lang.*;
import de.grogra.reflect.*;
import de.grogra.reflect.Method;
import de.grogra.reflect.Field;
import de.grogra.grammar.ASTWithToken;
import de.grogra.grammar.RecognitionExceptionList;
import de.grogra.xl.compiler.scope.*;
import de.grogra.xl.compiler.pattern.*;
import de.grogra.xl.compiler.scope.Local;
import de.grogra.xl.compiler.scope.Package;
import de.grogra.xl.modules.Instantiator;
import de.grogra.xl.property.CompiletimeModel.Property;

public interface CompilerTokenTypes {
	int EOF = 1;
	int NULL_TREE_LOOKAHEAD = 3;
	int BOOLEAN_LITERAL = 4;
	int INT_LITERAL = 5;
	int LONG_LITERAL = 6;
	int FLOAT_LITERAL = 7;
	int DOUBLE_LITERAL = 8;
	int CHAR_LITERAL = 9;
	int STRING_LITERAL = 10;
	int IDENT = 11;
	int COMPILATION_UNIT = 12;
	int PACKAGE = 13;
	int MODULE = 14;
	int SCALE = 15;
	int CLASS = 16;
	int INTERFACE = 17;
	int IMPORT_ON_DEMAND = 18;
	int STATIC_IMPORT_ON_DEMAND = 19;
	int SINGLE_TYPE_IMPORT = 20;
	int SINGLE_STATIC_IMPORT = 21;
	int EXTENDS = 22;
	int IMPLEMENTS = 23;
	int PARAMETERS = 24;
	int PARAMETER_DEF = 25;
	int SUPER = 26;
	int ARGLIST = 27;
	int SLIST = 28;
	int INSTANTIATOR = 29;
	int METHOD = 30;
	int THROWS = 31;
	int SEMI = 32;
	int CONSTRUCTOR = 33;
	int VARIABLE_DEF = 34;
	int ASSIGN = 35;
	int ARRAY_DECLARATOR = 36;
	int DECLARING_TYPE = 37;
	int VOID_ = 38;
	int BOOLEAN_ = 39;
	int BYTE_ = 40;
	int SHORT_ = 41;
	int CHAR_ = 42;
	int INT_ = 43;
	int LONG_ = 44;
	int FLOAT_ = 45;
	int DOUBLE_ = 46;
	int INSTANCE_INIT = 47;
	int STATIC_INIT = 48;
	int EMPTY = 49;
	int QUERY = 50;
	int EXTENT_VISIBILITY = 51;
	int COMPOUND_PATTERN = 52;
	int LABEL = 53;
	int PATTERN_WITH_BLOCK = 54;
	int MINIMAL = 55;
	int LATE_MATCH = 56;
	int SINGLE_MATCH = 57;
	int OPTIONAL_MATCH = 58;
	int SINGLE_OPTIONAL_MATCH = 59;
	int ANY = 60;
	int FOLDING = 61;
	int SEPARATE = 62;
	int LT = 63;
	int GT = 64;
	int LINE = 65;
	int LEFT_RIGHT_ARROW = 66;
	int PLUS_LEFT_ARROW = 67;
	int PLUS_ARROW = 68;
	int PLUS_LINE = 69;
	int PLUS_LEFT_RIGHT_ARROW = 70;
	int SLASH_LEFT_ARROW = 71;
	int SLASH_ARROW = 72;
	int SLASH_LINE = 73;
	int SLASH_LEFT_RIGHT_ARROW = 74;
	int TYPE_PATTERN = 75;
	int WRAPPED_TYPE_PATTERN = 76;
	int NAME_PATTERN = 77;
	int TREE = 78;
	int CONTEXT = 79;
	int EXPR = 80;
	int ROOT = 81;
	int METHOD_PATTERN = 82;
	int METHOD_CALL = 83;
	int DOT = 84;
	int APPLICATION_CONDITION = 85;
	int PARAMETERIZED_PATTERN = 86;
	int SUB = 87;
	int LEFT_ARROW = 88;
	int ARROW = 89;
	int X_LEFT_RIGHT_ARROW = 90;
	int TRAVERSAL = 91;
	int QUESTION = 92;
	int MUL = 93;
	int ADD = 94;
	int RANGE_EXACTLY = 95;
	int RANGE_MIN = 96;
	int RANGE = 97;
	int RULE = 98;
	int DOUBLE_ARROW_RULE = 99;
	int EXEC_RULE = 100;
	int PRODUCE = 101;
	int WITH = 102;
	int UNARY_PREFIX = 103;
	int TYPECAST = 104;
	int TYPECHECK = 105;
	int COM = 106;
	int NOT = 107;
	int NEG = 108;
	int POS = 109;
	int DIV = 110;
	int REM = 111;
	int POW = 112;
	int SHL = 113;
	int SHR = 114;
	int USHR = 115;
	int LE = 116;
	int GE = 117;
	int CMP = 118;
	int NOT_EQUALS = 119;
	int EQUALS = 120;
	int OR = 121;
	int XOR = 122;
	int AND = 123;
	int COR = 124;
	int CAND = 125;
	int ARRAY_INIT = 126;
	int RULE_BLOCK = 127;
	int ELIST = 128;
	int SHELL_BLOCK = 129;
	int THIS = 130;
	int QUALIFIED_SUPER = 131;
	int IF = 132;
	int RETURN = 133;
	int YIELD = 134;
	int THROW = 135;
	int SYNCHRONIZED_ = 136;
	int ASSERT = 137;
	int LABELED_STATEMENT = 138;
	int BREAK = 139;
	int CONTINUE = 140;
	int TRY = 141;
	int CATCH = 142;
	int FINALLY = 143;
	int NODES = 144;
	int NODE = 145;
	int LCLIQUE = 146;
	int RCLIQUE = 147;
	int FOR = 148;
	int ENHANCED_FOR = 149;
	int WHILE = 150;
	int DO = 151;
	int SWITCH = 152;
	int SWITCH_GROUP = 153;
	int CASE = 154;
	int DEFAULT = 155;
	int NULL_LITERAL = 156;
	int INVALID_EXPR = 157;
	int LONG_LEFT_ARROW = 158;
	int LONG_ARROW = 159;
	int LONG_LEFT_RIGHT_ARROW = 160;
	int INSTANCEOF = 161;
	int CLASS_LITERAL = 162;
	int QUOTE = 163;
	int ADD_ASSIGN = 164;
	int SUB_ASSIGN = 165;
	int MUL_ASSIGN = 166;
	int DIV_ASSIGN = 167;
	int REM_ASSIGN = 168;
	int POW_ASSIGN = 169;
	int SHR_ASSIGN = 170;
	int USHR_ASSIGN = 171;
	int SHL_ASSIGN = 172;
	int AND_ASSIGN = 173;
	int XOR_ASSIGN = 174;
	int OR_ASSIGN = 175;
	int DEFERRED_ASSIGN = 176;
	int DEFERRED_RATE_ASSIGN = 177;
	int DEFERRED_ADD = 178;
	int DEFERRED_SUB = 179;
	int DEFERRED_MUL = 180;
	int DEFERRED_DIV = 181;
	int DEFERRED_REM = 182;
	int DEFERRED_POW = 183;
	int DEFERRED_OR = 184;
	int DEFERRED_AND = 185;
	int DEFERRED_XOR = 186;
	int DEFERRED_SHL = 187;
	int DEFERRED_SHR = 188;
	int DEFERRED_USHR = 189;
	int INC = 190;
	int DEC = 191;
	int POST_INC = 192;
	int POST_DEC = 193;
	int IN = 194;
	int GUARD = 195;
	int ARRAY_ITERATOR = 196;
	int QUERY_EXPR = 197;
	int INVOKE_OP = 198;
	int QUALIFIED_NEW = 199;
	int INDEX_OP = 200;
	int NEW = 201;
	int DIMLIST = 202;
	int MODIFIERS = 203;
	int ANNOTATION = 204;
	int PRIVATE_ = 205;
	int PUBLIC_ = 206;
	int PROTECTED_ = 207;
	int STATIC_ = 208;
	int TRANSIENT_ = 209;
	int FINAL_ = 210;
	int ABSTRACT_ = 211;
	int NATIVE_ = 212;
	int VOLATILE_ = 213;
	int STRICT_ = 214;
	int ITERATING_ = 215;
	int CONST_ = 216;
	int GLOBAL_ = 217;
	int VARARGS_ = 218;
	int STATIC_MEMBER_CLASSES = 219;
	int MARKER = 220;
	int SINGLE_ELEMENT = 221;
	int NORMAL = 222;
}
