module xl.vmx {
	exports de.grogra.xl.vmx;

	requires xl;
	requires xl.core;
	requires java.desktop;
}